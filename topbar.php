
        <div class="wk_topbar">
            <div class="wk_wrap">
                <div class="wk_pulsante_modale" data-modale="wk_c2a_newsletter">
                    <!-- ICONA NEWSLETTER -->
                    <svg data-name="Livello 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 62.1"><path class="cls-1" d="M88.42,19H11.58A6.59,6.59,0,0,0,5,25.53V74.47a6.59,6.59,0,0,0,6.58,6.58H88.42A6.59,6.59,0,0,0,95,74.47V25.53A6.59,6.59,0,0,0,88.42,19Zm3,6.58V74.47a3.05,3.05,0,0,1-.05.52L65.28,50,91.35,25A2.84,2.84,0,0,1,91.4,25.53Zm-3-3,.27,0L50,59.67,11.31,22.58l.27,0ZM8.65,75a3.05,3.05,0,0,1-.05-.52V25.53A2.84,2.84,0,0,1,8.65,25L34.72,50Zm2.93,2.46a2.06,2.06,0,0,1-.26,0l26-24.93,11.43,11a1.81,1.81,0,0,0,2.5,0l11.43-11,26,24.93a2.06,2.06,0,0,1-.26,0Z" transform="translate(-5 -18.95)"/></svg>
                    <span>Iscriviti alla newsletter</span>
                </div>
                <div class="wk_pulsante_modale" data-modale="wk_c2a_professional">
                    <!-- ICONA COMMUNITY -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.12 60.61"><path class="cls-1" d="M63.48,80.22H5.94V72.15A5.7,5.7,0,0,1,8,67.71,92.81,92.81,0,0,1,24.88,56.89a.53.53,0,0,0,.3-.47V50.9a13.15,13.15,0,0,1-1.64-4.44c-.62-.06-1.45-.89-2.33-4.06-1.22-4.29.07-4.93,1.16-4.82a10.85,10.85,0,0,1-.47-1.78h0a12.49,12.49,0,0,1,0-5.64,12.23,12.23,0,0,1,3.22-5.84,15.73,15.73,0,0,1,3.05-2.53,12.19,12.19,0,0,1,3-1.52h0a10.1,10.1,0,0,1,2.66-.51,11.06,11.06,0,0,1,6.77,1.43,8.4,8.4,0,0,1,3.36,3.06s5.47.39,3.62,11.48a12,12,0,0,1-.47,1.78c1.09-.11,2.38.53,1.16,4.82-.89,3.13-1.71,4-2.33,4.06a13,13,0,0,1-1.64,4.44v5.64a.52.52,0,0,0,.29.47A93.37,93.37,0,0,1,61.42,67.78a5.72,5.72,0,0,1,2.06,4.44ZM92.15,68.77a85.33,85.33,0,0,0-15.5-10,.48.48,0,0,1-.27-.44V53.16a12,12,0,0,0,1.46-4c.58,0,1.34-.89,2.15-3.74,1.12-4-.07-4.54-1.07-4.44a10.68,10.68,0,0,0,.4-1.67C81.1,29,76,28.69,76,28.69a7.74,7.74,0,0,0-3-2.83,10.15,10.15,0,0,0-6.22-1.31,9.07,9.07,0,0,0-2.47.47h0a10.71,10.71,0,0,0-2.74,1.39,15.28,15.28,0,0,0-2.81,2.32,11.46,11.46,0,0,0-3,5.33,11.75,11.75,0,0,0,0,5.21h0a9.93,9.93,0,0,0,.44,1.62c-1-.09-2.2.49-1.08,4.45.83,2.89,1.59,3.69,2.17,3.78a12.1,12.1,0,0,0,1.51,4.12V58.4a.47.47,0,0,1-.27.43l-.78.38a.48.48,0,0,0-.2.66.52.52,0,0,0,.15.18c2,1.39,4.19,3,6.29,4.68a9.67,9.67,0,0,1,3.55,7.52v8.06H94.06V72.83A5.27,5.27,0,0,0,92.15,68.77Z" transform="translate(-5.94 -19.69)"/></svg>
                    <span>Si un consulente finanziario?</span>
                </div>
                <div class="wk_pulsante_modale" data-modale="wk_c2a_abbonati">
                    <!-- ICONA MAGAZINE -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.13 74.28"><path class="cls-1" d="M30.53,87.14l50.54-1.06-1.88-5.42C72.41,83,55.38,85.88,30.53,87.14Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M37.69,84.72c14.7-1.44,34.73-4.2,43.38-8.12l-3-7.91C68.66,75.42,50.85,81.14,37.69,84.72Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M65.08,12.86S62,20.34,50,27.3c-8.2,4.76-21.27,8-21.27,8l-9.8,50.79s22.6-6.63,39.71-18.31c16.24-11.08,18.56-17,18.56-17ZM54,48.11c-8,4.65-19,8.16-25,9.86l3.66-19c4.49-1.27,13.26-4,19.59-7.7a52.06,52.06,0,0,0,11.08-8.44l4.67,14.61A67.51,67.51,0,0,1,54,48.11Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M60.38,70.33c-5.6,3.82-15.29,8.6-21.05,11.3C51.42,78.15,72,70.54,79,63.91l-2.8-6.64C73.36,60.38,68.53,64.76,60.38,70.33Z" transform="translate(-18.93 -12.86)"/></svg>
                    <span>Abbonati alle riviste</span>
                </div>
            </div>
        </div>