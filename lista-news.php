<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-touch-fullscreen" content="YES">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes ">
    <title>Advisoronline</title>
    <meta charset="utf-8"/>

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@600&display=swap" rel="stylesheet">



    <!-- CSS -->
    <?php $random_asset=rand(0,1000); ?>
    <link rel="stylesheet" href="/css/atf.css?v=<?= $random_asset ?>">
</head>
<body>
<div id="mainwrap">
	<?php include('header.php');?>
	<div id="wrapcontent">	

		<!-- riusciamo ad aggiungere qui una classe wk_listato? per le pagine contenenti liste di articoli? -->
		<div class="wrapcontent_padding articolo-container articolo-single-page wk_wrap wk_mainwrap wk_listato">

			<div id="main" class="leftColumnPos ">
				<div id="col41065" class="column_generico homeColumn0 lastPBColumn"> 
					<div class="column_padding">
						<?php include('block_breadcrumbs.php');?>
						<?php include('block_intro_listato.php');?>
						<?php include('block_listato.php');?>
						
					</div><!-- chiusura column padding -->
				</div>
			</div><!-- chiusura MAIN -->

			<?php include('block_sidebar.php');?>

			
			<?php include('block_advisorprivate_inner.php');?>
			<?php include('block_advisorprofessional_inner.php');?>

			<!-- BLOCCO ADVISOR PLAY -->

		</div><!-- chiusura articolo-container -->
	</div><!-- chiusura wrapcontent -->
	<?php include('footer.php');?>
</div><!-- chiusura MAINWRAP -->
</body>
</html>