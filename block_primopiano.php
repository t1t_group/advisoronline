<!--- INIZIO PRIMO PIANO -->

<div class="wrapwidg wk_widget" id="primo-piano">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<!-- DEFINIRE LINK A NEWS PIU LETTE -->
			<a href="#">Primo piano</a>
		</h2>

		<div id="lastContents2" class="wk_contenitore_lista">

			<div class="contents_container_all">

				<!-- ELEMENTO IN EVIDENZA -->
				<div class="wk_main_primo_piano">
					<div class="wk_wrap_image">
	  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
	  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/55425_consulentipng_highlight.png');" class="wk_image"></a>
	  				</div>
		  			<div class="wk_articolo">
		  				<span class="wk_categoria">Strumenti finanziari</span>
		  				<h4 class="wk_titolo">
		  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
		  						Coronavirus, l’impatto sul settore del lusso.
		  					</a>
		  				</h4>
		  				<div class="wk_excerpt">L’a.d. Gian Maria Mossa: “No a possibili nozze con Mediolanum o Fineco. Andiamo tutti quanti avanti per la nostra strada”</div>
		  				<span class="wk_meta">27/03/2020 | <a href="#">Redazione</a></span>
		  				<?php include('block_socialsharing.php');?>
		  			</div>
				</div>
				<!-- FINE ELEMENTO IN EVIDENZA -->

				<!-- CICLO NEWS SECONDARIE IN CAROUSEL -->
				<div class="wk-owl-carousel owl-carousel wk-owl-primo-piano">

					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					$conteggio_coppie=0;
					for($i=0; $i<=7; $i++){

						// VANNO WRAPPATI a 2 a 2 PER CONSENTIRE LA VISUALIZZAZIONE RESPONSIVE
						
						if($i%2==0){
							$conteggio_coppie++;

							if($conteggio_coppie%2==0){
								?><div class="wk_item"><?php
							}else{
								?><div class="wk_item even"><?php
							}
						}
						?>
						  	<div class="wk_sub_item">
	  							<div class="wk_wrap_image">
	  			  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
	  			  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/55425_consulentipng_highlight.png');" class="wk_image"></a>
	  			  				</div>
  					  			<div class="wk_articolo">
  					  				<span class="wk_categoria"><?= $i; ?> Asset manager</span>
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Duccio Marconi: “Il CF partner nell’educazione finanziaria”
  					  					</a>
  					  				</h4>
  					  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
  					  				<?php include('block_socialsharing.php');?>
  					  			</div>
	  						</div>
						<?php

						if($i%2==1){
							// CHIUSURA WK_ITEM OGNI DUE ELEMENTI
							?></div><?php
						}
					}

					?>
				</div>

					


			</div>

			<div class="customContentListFooter">
				
			</div>

		</div>	
	</div>
</div>


<!--- FINE PRIMO PIANO -->