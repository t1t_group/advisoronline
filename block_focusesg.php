<!--- INIZIO STRUMENTI FINANZIARI -->


<div class="wrapwidg wk_widget" id="focus-esg">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<!-- LINK FOCUS ESG -->
			<a href="#" title="Focus sostenibilità ESG">Focus sostenibilità ESG</a>
			<!-- LINK INVESTMENT PARTNER -->
			<div class="wk_logo_nn">
				<a href="#" title="NN investment partners">
					<img src="/img/logo_nn.png" alt="NN investment partners" title="NN investment partners">
				</a>
			</div>
		</h2>

		<div id="lastContents" class="wk_contenitore_lista">

			<div class="contents_container_all">

				<div class="wk-owl-carousel owl-carousel wk-owl-focus-esg">

					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					$conteggio_coppie=0;
					for($i=0; $i<=11; $i++){

						// VANNO WRAPPATI a 2 a 2 PER CONSENTIRE LA VISUALIZZAZIONE RESPONSIVE
						
						if($i%2==0){
							$conteggio_coppie++;

							if($conteggio_coppie%2==0){
								?><div class="wk_item"><?php
							}else{
								?><div class="wk_item even"><?php
							}
						}
						?>
						  	<div class="wk_sub_item">
						  		<!-- LINK A PAGINA PARTNER -->
						  		<div class="wk_wrap_image">
	  			  					<a href="#"
	  			  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/AP14_focus-cover.jpg');" class="wk_image"></a>
	  			  				</div>
  					  			<div class="wk_articolo">
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Rating ESG, come non cadere in errore
  					  					</a>
  					  				</h4>
  					  				<div class="wk_excerpt">Un’agenzia di rating ha considerato una società tradizionale Oil&Gas più sostenibile di ...</div>
  					  			</div>
	  						</div>
						<?php

						if($i%2==1){
							// CHIUSURA WK_ITEM OGNI DUE ELEMENTI
							?></div><?php
						}
					}

					?>
				</div>


			</div>

			<div class="customContentListFooter">
				<a href="" class="wk_pulsante" title="Tutti i focus ESG" target="_blank">Scopri tutti i focus</a>
			</div>

		</div>	
	</div>
</div>


<!--- FINE STRUMENTI FINANZIARI -->
