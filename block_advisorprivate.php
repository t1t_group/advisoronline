<!--- INIZIO ADVISOR PROFESSIONAL -->



<div class="wrapwidg wk_widget" id="advisor-private">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<a href="/tag/view-tags(primo+piano).action">Advisor private</a>
		</h2>

		<div id="lastContents" class="wk_contenitore_lista">

			<div class="contents_container_all">


					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					for($i=0; $i<=3; $i++){
						?>
						  	<div class="wk_item">
						  		<div class="wk_wrap_image">
	  			  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
	  			  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/55500_highlight.png');" class="wk_image"></a>
	  			  				</div>
  					  			<div class="wk_articolo">
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Coronavirus, le ricadute 
sull’economia globale
  					  					</a>
  					  				</h4>
  					  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
  					  			</div>
	  						</div>
						<?php
					}

					?>

			</div>

			<div class="customContentListFooter">
				<!-- INSERIRE LINK A PAGINA ADVISOR PRIVATE -->
				<a href="" class="wk_pulsante" title="Vai a Advisor Private" target="_blank">ENTRA NEL MONDO PRIVATE</a>
				<a title="Vai a Advisor Private" target="_blank" href="#" class="wk_link_piattaforma">
					<svg x="0px" y="0px"
						 viewBox="0 0 413.3 44.9" style="enable-background:new 0 0 413.3 44.9;" xml:space="preserve">
					<style type="text/css">
						.wk_private_color{fill:#9CB5A9;}
					</style>
					<g>
						<g>
							<g>
								<g>
									<path d="M29.1,42.6l-1.8-5.9H15l-2,5.9H2.9L17.3,2.9h7.5l14.4,39.7H29.1z M21.3,18l-3.6,10.6h6.9L21.3,18z"/>
								</g>
								<g>
									<path class="wk_private_color" d="M54,28.5h-6v14.2h-9.8V2.9H54c8.8,0,13.6,6.2,13.6,12.8C67.5,22.2,62.8,28.5,54,28.5z M53.5,11.7H48v8h5.5
										c2.7,0,4.2-2,4.2-4C57.8,13.6,56.2,11.7,53.5,11.7z"/>
								</g>
							</g>
							<g>
								<path class="wk_private_color" d="M98.7,38.3l-1.7-5.1H86.5l-1.8,5.1h-6L89.4,9.5h4.5l10.7,28.8H98.7z M91.8,18l-3.7,10.5h7.3L91.8,18z"/>
								<path class="wk_private_color" d="M125.8,35.6c-1.9,1.9-4.7,2.8-7.8,2.8h-10.6V9.5h10.6c3.1,0,5.8,0.9,7.8,2.8c3.3,3.2,3,7.2,3,11.5
									S129.1,32.3,125.8,35.6z M121.8,16.3c-1-1.2-2.3-1.8-4.3-1.8h-4.3v18.8h4.3c2,0,3.3-0.6,4.3-1.8c1.1-1.3,1.2-3.4,1.2-7.7
									C123.1,19.4,122.9,17.6,121.8,16.3z"/>
								<path class="wk_private_color" d="M144.2,38.3h-4.3l-9.7-28.8h5.9l5.9,18.8l5.9-18.8h6L144.2,38.3z"/>
								<path class="wk_private_color" d="M156.7,38.3V9.5h5.7v28.8H156.7z"/>
								<path class="wk_private_color" d="M176.5,38.6c-4.5,0-7.8-0.9-10.7-3.8l3.7-3.6c1.9,1.8,4.4,2.4,7.1,2.4c3.4,0,5.2-1.3,5.2-3.6
									c0-1-0.3-1.9-0.9-2.4c-0.6-0.5-1.2-0.8-2.6-1l-3.5-0.5c-2.5-0.4-4.4-1.1-5.7-2.4c-1.4-1.4-2.1-3.2-2.1-5.7
									c0-5.1,3.9-8.7,10.2-8.7c4,0,7,1,9.5,3.4l-3.6,3.5c-1.9-1.7-4.1-2-6-2c-3.1,0-4.5,1.7-4.5,3.6c0,0.7,0.2,1.5,0.9,2.1
									c0.6,0.5,1.5,1,2.7,1.1l3.5,0.5c2.7,0.4,4.4,1.1,5.6,2.2c1.6,1.5,2.3,3.6,2.3,6.1C187.4,35.4,182.6,38.6,176.5,38.6z"/>
								<path class="wk_private_color" d="M209.8,35.4c-2.1,2-4.6,3.2-7.9,3.2s-5.9-1.1-8-3.2c-3-2.9-2.9-6.5-2.9-11.5c0-5-0.1-8.6,2.9-11.5
									c2.1-2,4.6-3.2,8-3.2s5.9,1.1,7.9,3.2c3,2.9,2.9,6.5,2.9,11.5C212.7,28.9,212.7,32.5,209.8,35.4z M205.5,15.8
									c-0.8-0.9-2.1-1.5-3.7-1.5c-1.6,0-2.9,0.6-3.7,1.5c-1.1,1.2-1.4,2.5-1.4,8.1c0,5.5,0.3,6.9,1.4,8.1c0.8,0.9,2.1,1.5,3.7,1.5
									c1.6,0,2.8-0.6,3.7-1.5c1.1-1.2,1.4-2.5,1.4-8.1C207,18.4,206.6,17,205.5,15.8z"/>
								<path class="wk_private_color" d="M233.4,38.3l-5.7-11.5h-4.1v11.5h-5.7V9.5h11.5c6,0,9.5,4,9.5,8.8c0,4-2.5,6.6-5.4,7.6l6.6,12.4H233.4z
									 M228.9,14.5h-5.4v7.6h5.4c2.5,0,4.2-1.6,4.2-3.8C233.1,16.1,231.4,14.5,228.9,14.5z"/>
							</g>
							<g>
								<path d="M264.2,27.5h-5.6v10.8h-5.7V9.5h11.4c6,0,9.7,4.1,9.7,9C273.9,23.4,270.3,27.5,264.2,27.5z M263.9,14.5h-5.3v7.9h5.3
									c2.6,0,4.2-1.6,4.2-3.9C268.2,16.1,266.5,14.5,263.9,14.5z"/>
								<path d="M292.6,38.3l-5.7-11.5h-4.1v11.5h-5.7V9.5h11.5c6,0,9.5,4,9.5,8.8c0,4-2.5,6.6-5.3,7.6l6.5,12.4H292.6z M288.2,14.5h-5.4
									v7.6h5.4c2.5,0,4.2-1.6,4.2-3.8C292.3,16.1,290.7,14.5,288.2,14.5z"/>
								<path d="M302.4,38.3V9.5h5.7v28.8H302.4z"/>
								<path d="M324,38.3h-4.3L310,9.5h5.9l5.9,18.8l5.9-18.8h6L324,38.3z"/>
								<path d="M350.8,38.3l-1.7-5.1h-10.4l-1.8,5.1h-6l10.7-28.8h4.5l10.7,28.8H350.8z M344,18l-3.7,10.5h7.3L344,18z"/>
							</g>
							<g>
								<path d="M368.1,14.5v23.8h-5.6V14.5H355v-5h20.7v5H368.1z"/>
								<path d="M378.6,38.3V9.5h19v5h-13.4v6.8h11.4v5h-11.4v7h13.4v5H378.6z"/>
							</g>
						</g>
						<g>
							<path d="M407.2,29.8c-2.7,0-4.3,1.7-4.3,4.3c0,2.6,1.6,4.3,4.3,4.3c2.7,0,4.3-1.7,4.3-4.3C411.5,31.5,409.9,29.8,407.2,29.8
								 M407.2,37.7c-2.2,0-3.6-1.3-3.6-3.7c0-2.4,1.5-3.7,3.6-3.7c2.1,0,3.6,1.3,3.6,3.7C410.8,36.5,409.3,37.7,407.2,37.7"/>
							<path d="M408.9,33.2c0-0.8-0.6-1.3-1.6-1.3h-1.8v4.2h0.8v-1.4h0.9l0.8,1.4h0.9l-0.8-1.5C408.6,34.3,408.9,33.9,408.9,33.2
								 M407.2,34h-0.9v-1.5h1c0.5,0,0.8,0.3,0.8,0.7C408.1,33.8,407.8,34,407.2,34"/>
						</g>
					</g>
					</svg>
				</a>
			</div>

		</div>	
	</div>
</div>


<!--- FINE ADVISOR PROFESSIONAL -->