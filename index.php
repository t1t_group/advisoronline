<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-touch-fullscreen" content="YES">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes ">
    <title>Advisoronline</title>
    <meta charset="utf-8"/>

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@600&display=swap" rel="stylesheet">



    <!-- CSS -->
    <?php $random_asset=rand(0,1000); ?>
    <link rel="stylesheet" href="/css/atf.css?v=<?= $random_asset ?>">
</head>
<body>
	<div class="body_click" style="">
			<div id="div-gpt-ad-1481535471239-0" data-google-query-id="CPXF3pTEpOkCFYfydwodhI8Izg">
				<img src="/img/test_skin.jpg">
			</div>
	</div>

	<div id="mainwrap">

		<?php include('header.php');?>

		<div id="home-wrapcontent" class="leftColumnPos">

			
			<div class="wrapcontent_padding wk_wrap wk_mainwrap">

				<div id="main">

					<div id="col41065" class="column_generico homeColumn0 lastPBColumn"> 
 
						<div class="column_padding ">

							<?php include('block_fattidelgiorno.php');?>

							<?php include('block_newspiulette.php');?>

							<?php include('block_primopiano.php');?>

							<?php include('block_side_ads.php'); ?>

							<?php include('block_twitter.php'); ?>

							<?php include('block_giridipoltrone.php');?>

							<!-- andrebbero riuniti nello stesso wrapper -->

							<?php include('block_advisorprofessional.php');?>

							<?php include('block_side_ads.php'); ?>

							<?php include('block_advisorprivate.php');?>

							<!-- fine andrebbero riuniti nello stesso wrapper -->

					


							<?php include('block_advisorplay.php');?>

							<?php include('block_strumentifinanziari.php');?>

							<?php include('block_focusesg.php');?>

							<?php include('block_editoriale.php');?>

							<?php include('block_riviste.php');?>

							<?php include('block_side_ads.php'); ?>


						</div>

					</div>

				</div>

			</div>

		</div>

		<?php include('footer.php');?>

	</div>
</body>
</html>
