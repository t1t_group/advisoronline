var offset_1;
var distanza_sideads1;
var dimensione_twitter;
var offset_2;
var distanza_sideads2;
var banner_top;
var offset_sidebar;
var distanza_sidebar;


var lastScrollTop = 0;


jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        //alert(jQuery.easing.default);
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c*(t/=d)*t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c *(t/=d)*(t-2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t + b;
        return -c/2 * ((--t)*(t-2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c*(t/=d)*t*t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t*t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
        return c/2*((t-=2)*t*t*t*t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
        return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*(t/=d)*t*((s+1)*t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158; 
        if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
    }
});

/* DIMENSIONI SCHERMO */
	 
	var $width = $(window).width();
	var $height = $(window).height();
	var $docheight = document.documentElement.scrollHeight;
	var $scrollspace = $docheight-$height;

function is_touch_device() {
 return (('ontouchstart' in window)
      || (navigator.maxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
 //navigator.msMaxTouchPoints for microsoft IE backwards compatibility
}


$(document).ready(function() {
	
	
	if(!is_touch_device()){
		
		$(".touch").addClass("notouch");
		$("body").addClass("notouch");
		
	}else{
		$(".touch").addClass("istouch");
		$("body").addClass("istouch");
	}


	// GESTIONE MENU MOBILE
	if(is_touch_device()) {
		
		$("a.expandable").on('click', function(){
			event.preventDefault();
			$(this).toggleClass('attivo');
			$(this).siblings('.dropdown').slideToggle();
		});

		$(".hamburger-menu").on('click', function(){
			$(this).toggleClass('attivo');
			$("body").toggleClass("menu_aperto");
			$("html").toggleClass("bloccoscroll");
			$("body").toggleClass("bloccoscroll");
		});
	} 


	// ATTIVO IL MENU STICKY
	if($width<768) {
		if ($(window).scrollTop() >= 40) {
		    $('body').addClass('menu_attivo');
		}
		else {
		    $('body').removeClass('menu_attivo');
		}
	}else {
		if ($(window).scrollTop() >= 150) {
		    $('body').addClass('menu_attivo');
		}
		else {
		    $('body').removeClass('menu_attivo');
		}
	}


	$(window).scroll(function(){
		if($width<768) {
			if ($(window).scrollTop() >= 40) {
			    $('body').addClass('menu_attivo');
			}
			else {
			    $('body').removeClass('menu_attivo');
			}
		}else {
			if ($(window).scrollTop() >= 150) {
			    $('body').addClass('menu_attivo');
			}
			else {
			    $('body').removeClass('menu_attivo');
			}
		}
	});

	// GESTIONE MENU DA TABLET 
	if(is_touch_device()) {


	}

	/* FATTI DEL GIORNO CAROUSEL */

	if($('#fatti-del-giorno').length){
		var fatti_del_giorno_owl;
		
		fatti_del_giorno_owl=$('.wk-owl-fatti-del-giorno').owlCarousel({
		   loop:true,
		   margin:15,
	       responsiveClass:true,
	       autoplay:true,
	       autoplayHoverPause:true,
	       autoplayTimeout:3000,
	       dots:false,
	       nav:false,
	       responsive:{
	               0:{
	                   items:1,
	               },
	               600:{
	                   items:2
	               },
	               1000:{
	                   items:3
	               },
	               1260:{
	                   items:4
	               }
	           }
		});

		$('.wk_barra_scroll').on('click', function(){
			$('.wk-owl-fatti-del-giorno').trigger('next.owl.carousel');
		});

		fatti_del_giorno_owl.on('changed.owl.carousel', function(e) {
		        fatti_del_giorno_owl.trigger('stop.owl.autoplay');
		        fatti_del_giorno_owl.trigger('play.owl.autoplay');
		  });
	}



	/* PRIMO PIANO CAROUSEL */

	if($('#primo-piano').length){
		var primo_piano_owl;

		// RICAVO NUMERO ELEMENTI SPONSORED E NASCONDO PARTE DEI CONTENUTI
		var numero_sponsorizzati=$('#primo-piano .sponsored').length;

		if(numero_sponsorizzati>0 && numero_sponsorizzati<3){
			//elimino ultimo wk_item
			$('#primo-piano .wk_item:last').remove();

		}else if(numero_sponsorizzati>2){
			// elimino ultimi due elementi wk_item
			$('#primo-piano .wk_item:last').remove();
			$('#primo-piano .wk_item:last').remove();
		}



		
		primo_piano_owl=$('.wk-owl-primo-piano').owlCarousel({
		   loop:true,
   		   margin:0,
   	       responsiveClass:true,
   	       autoplay:true,
   	       autoplayHoverPause:true,
   	       autoplayTimeout:3000,
   	       dots:true,
   	       nav:false,
   	       responsive:{
   	               0:{
   	                   items:1,
   	               },
   	               1000:{
   	                   items:2,
   	               }
   	           }
		});

		
	}



		if($('#primo-piano.primo-piano-inner').length){
			var primo_piano_owl;
			
			primo_piano_owl=$('.wk-owl-primo-piano-inner').owlCarousel({
			   loop:true,
	   		   margin:0,
	   	       responsiveClass:true,
	   	       autoplay:true,
	   	       autoplayHoverPause:true,
	   	       autoplayTimeout:3000,
	   	       dots:true,
	   	       nav:false,
	   	       responsive:{
	   	               0:{
	   	                   items:1,
	   	               },
	   	               768:{
	   	                   items:2,
	   	               }
	   	           }
			});

			
		}


	/* GIRI DI POLTRONE CAROUSEL */

	if($('#giri-di-poltrone').length){
		var giri_di_poltrone_owl;
		
		giri_di_poltrone_owl=$('.wk-owl-giri-di-poltrone').owlCarousel({
		   loop:true,
   		   margin:0,
   	       responsiveClass:true,
   	       autoplay:true,
   	       autoplayHoverPause:true,
   	       autoplayTimeout:3000,
   	       dots:true,
   	       nav:false,
   	       loop:true,
   	       responsive:{
   	               0:{
   	                   items:2,
   	               },
   	               768:{
   	                   items:3,
   	               },
   	               1000:{
   	                   items:4,
   	               }
   	           }
		});

		
	}


	/* STRUMENTI FINANZIARI CAROUSEL */

	if($('#strumenti-finanziari').length){
		var giri_di_poltrone_owl;
		
		giri_di_poltrone_owl=$('.wk-owl-strumenti-finanziari').owlCarousel({
		   loop:true,
   		   margin:0,
   	       autoplay:true,
   	       autoplayHoverPause:true,
   	       autoplayTimeout:6000,
   	       dots:true,
   	       nav:false,
   	       responsive:{
   	               0:{
   	                   items:1,
   	               }
   	           }
		});

		
	}


	/* FOCUS ESG CAROUSEL */

	if($('#focus-esg').length){
		var primo_piano_owl;
		
		primo_piano_owl=$('.wk-owl-focus-esg').owlCarousel({
		   loop:true,
   		   margin:0,
   	       responsiveClass:true,
   	       autoplay:true,
   	       autoplayHoverPause:true,
   	       autoplayTimeout:3000,
   	       dots:true,
   	       nav:false,
   	       responsive:{
   	               0:{
   	                   items:1,
   	               },
   	               768:{
   	                   items:2,
   	               }
   	           }
		});

		
	}


	/* ADVISOR PLAY */

	if($('#advisor-play').length){
		var advisor_play_owl;
		
		advisor_play_owl=$('.wk-owl-advisor-play').owlCarousel({
		   loop:true,
   		   margin:0,
   	       autoplay:true,
   	       autoplayHoverPause:true,
   	       autoplayTimeout:3000,
   	       dots:true,
   	       nav:false,
   	       responsive:{
   	               0:{
   	                   items:1,
   	               },
   	           }
		});

		
	}


	/* GESTIONE SOCIAL SHARE NASCOSTO */

	$('.wk_social_expand').on('click', function(){
		$(this).parents('.wk_social_sharing').toggleClass('attivo');
	});
	

	/* GESTIONE MODALI */
	$('.wk_pulsante_modale').on('click', function(){

		if($(this).hasClass('attivo')){
			$(this).removeClass('attivo');
			$('.wk_modale').removeClass('attivo');
		}else {
			$("body").addClass("overlay");
			$(this).addClass('attivo');
			$('.wk_modale').slideToggle();
			$('.wk_modale').addClass('overlay-bg');
			var modale=$(this).attr('data-modale');
			$('.'+modale).toggleClass('attivo');
		}
		
	});



	$('.network_action').on('click', function(){
		if($(this).hasClass('attivo')){
			$(this).removeClass('attivo');
			$('.wk_modale').removeClass('attivo');
		}else {
			$("body").addClass("overlay");
			$(this).addClass('attivo');
			$('.wk_modale').slideToggle();
			$('.wk_modale').addClass('overlay-bg');
			var modale=$(this).attr('data-modale');
			$('.'+modale).toggleClass('attivo');
		}
		
	});

	$(".wk_modale").on("click", function(e){
		if($(e.target).hasClass("overlay-bg")){
	        $("body").removeClass("overlay");
	        $(".wk_modale").slideToggle();
	        $('.wk_modale').removeClass('overlay-bg');
	        $('.wk_pulsante_modale').removeClass('attivo');
	        $('.wk_modale').removeClass('attivo');
	        $('.network_action').removeClass('attivo');
		}
	});

	$('.wk_modale:not(.wk_modale_video) .fa-times').on('click', function(){
		$("body").removeClass("overlay");
		$(".wk_modale").slideToggle();
		$('.wk_modale').removeClass('overlay-bg');
		$('.wk_pulsante_modale').removeClass('attivo');
		$('.wk_modale').removeClass('attivo');
		$('.network_action').removeClass('attivo');
	});


	/* FLEXSLIDER MODALI */

	if($(".wk_modale_wrap .slider").length > 0){
		$('.wk_modale_wrap .slider').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    controlNav: true, 

		});
	}


	if($("body.newsletter .slider").length > 0){
		$('body.newsletter .slider').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    controlNav: true, 

		});
	}


	/* GESTIONE ADS */
	/* Sticky SKIN & billboard */

	$(window).scroll(function(){
		if($('body').hasClass( "has-backskin" )){
			if($width>1000) {
				if ($(window).scrollTop() >= 300) {
				    $('body').addClass('backskin_attiva');
				}
				else {
				    $('body').removeClass('backskin_attiva');
				}
			}
		}else{

			if($width<768) {
				if ($(window).scrollTop() >= 20) {
				    $('body').addClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeOut(3000, 'easeInExpo' ); /*  "easyInExpo"*/
				}
				else {
				    $('body').removeClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeIn(1000);
				}
			}else if($width<1000) {
				if ($(window).scrollTop() >= 180) {
				    $('body').addClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeOut(3000, 'easeInExpo' ); /*  "easyInExpo"*/
				}
				else {
				    $('body').removeClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeIn(1000);
				}
			}else {
				if ($(window).scrollTop() >= 170) {
				    $('body').addClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeOut(3000, 'easeInExpo' ); /*  "easyInExpo"*/
				    $('.wk_ads_orizzontale_1_copy').show();

				}
				else {
				    $('body').removeClass('billboard_attiva');
				    $(".wk_ads_orizzontale_1").fadeIn(1000);
				    $('.wk_ads_orizzontale_1_copy').hide();
				}
			}
		}
	});


	/* NEWS PIU LETTE ADS */


	$('#wk_footer_menu').on('change', function () {
	  var url = $(this).val(); // get selected value
	  if (url) { // require a URL
	      window.location = url; // redirect
	  }
	  return false;
	});





});	/* FINE DOCUMENT READY */

$(window).on("load", function() {


	// CALCOLO DIMENSIONI DELLE ADS NELLE SIDEBAR IN HOME
	if($('#wk_sideads1').length && $('#wk_sideads2').length ){

		offset_1=calcolaSideads1_custom();
		distanza_sideads1=$("#wk_sideads1").offset().top;

		offset_2=calcolaSideads2_custom();
		distanza_sideads2=$("#wk_sideads2").offset().top;

	}


	// CALCOLO DIMENSIONI DELLE ADS NELLA SIDEBAR

	if($('#sidebar').length ){

		offset_sidebar=calcola_sidebar_custom();
		distanza_sidebar=$("#sidebar").offset().top;

	}




    $(window).scroll(function(){
    	$valore_scroll=$(window).scrollTop();

    	if ($valore_scroll > lastScrollTop ){
    	       // downscroll code

    	       //if($('body').hasClass( "has-backskin" )){
    	       	if($width>1000) {
    	       		

    	       		// PRIMA SIDEBAR ADS
    	       		if(offset_1 > 0){
	    	       		if ($valore_scroll >= distanza_sideads1 - 65 &&  $valore_scroll < offset_1 + distanza_sideads1 ) {
	    	       			$("#wk_sideads1 > .widget_padding").addClass('wk_sticky').css('top', '68px');
	    	       		}else if( $valore_scroll >= offset_1 + distanza_sideads1){
	    	       			$top=offset_1 + 65;
	    	       			$("#wk_sideads1 > .widget_padding").addClass('wk_post').css('top', $top+'px');
	    	       		}else if($valore_scroll < distanza_sideads1 ){
	    	       			$("#wk_sideads1 > .widget_padding").removeClass('wk_sticky');
	    	       			$("#wk_sideads1 > .widget_padding").removeClass('wk_post').css('top', '0px');
	    	       		}
	    	       	}

	    	       	// SECONDA SIDEBAR ADS
	    	       	if(offset_2 > 0){
	    	       		if ($valore_scroll >= distanza_sideads2 - 65 &&  $valore_scroll < offset_2 + distanza_sideads2 ) {
	    	       			$("#wk_sideads2 > .widget_padding").addClass('wk_sticky').css('top', '68px');
	    	       		}else if( $valore_scroll >= offset_2 + distanza_sideads2){
	    	       			$top=offset_2 + 65;
	    	       			$("#wk_sideads2 > .widget_padding").addClass('wk_post').css('top', $top+'px');
	    	       		}else if($valore_scroll < distanza_sideads2 ){
	    	       			$("#wk_sideads2 > .widget_padding").removeClass('wk_sticky');
	    	       			$("#wk_sideads2 > .widget_padding").removeClass('wk_post').css('top', '0px');
	    	       		}
    	       		}


    	       		// SIDEBAR PAGINE INTERNE
	    	       	
	    	       	if(offset_sidebar > 0){
	    	       		if ($valore_scroll >= distanza_sidebar - 65 &&  $valore_scroll < offset_sidebar + distanza_sidebar ) {
	    	       			$("#sidebar > .column_padding").addClass('wk_sticky').css('top', '68px');
	    	       		}else if( $valore_scroll >= offset_sidebar + distanza_sidebar){
	    	       			$top=offset_sidebar + 65;
	    	       			$("#sidebar > .column_padding").addClass('wk_post').css('top', $top+'px');
	    	       		}else if($valore_scroll < distanza_sideads2 ){
	    	       			$("#sidebar > .column_padding").removeClass('wk_sticky');
	    	       			$("#sidebar > .column_padding").removeClass('wk_post').css('top', '0px');
	    	       		}
		       		}
		       		

    	       	}
    	       //}else{
    	       	
    	       //}
    	} else {
    	      // upscroll code
    	      //if($('body').hasClass( "has-backskin" )){
      	       	if($width>1000) {
      	       		
      	       		// SIDEBAR ADS 1
      	       		if ($valore_scroll >= distanza_sideads1 - 65 &&  $valore_scroll < offset_1 + distanza_sideads1 ) {
      	       			$("#wk_sideads1 > .widget_padding").addClass('wk_sticky').css('top', '68px');
      	       			$("#wk_sideads1 > .widget_padding").removeClass('wk_post');

      	       		}else if( $valore_scroll >= offset_1 + distanza_sideads1){
      	       			

      	       		}else if($valore_scroll < distanza_sideads1 ){
      	       			$("#wk_sideads1 > .widget_padding").removeClass('wk_sticky');
      	       			$("#wk_sideads1 > .widget_padding").removeClass('wk_post').css('top', '0px');
      	       		}

      	       		// SIDEBAR ADS 2
      	       		if ($valore_scroll >= distanza_sideads2 - 65 &&  $valore_scroll < offset_2 + distanza_sideads2 ) {
      	       			$("#wk_sideads2 > .widget_padding").addClass('wk_sticky').css('top', '68px');
      	       			$("#wk_sideads2 > .widget_padding").removeClass('wk_post');

      	       		}else if( $valore_scroll >= offset_2 + distanza_sideads2){
      	       			

      	       		}else if($valore_scroll < distanza_sideads2 ){
      	       			$("#wk_sideads2 > .widget_padding").removeClass('wk_sticky');
      	       			$("#wk_sideads2 > .widget_padding").removeClass('wk_post').css('top', '0px');
      	       		}

      	       		// SIDEBAR INTERNA
      	       		if ($valore_scroll >= distanza_sidebar - 65 &&  $valore_scroll < offset_sidebar + distanza_sidebar ) {
      	       			$("#sidebar > .column_padding").addClass('wk_sticky').css('top', '68px');
      	       			$("#sidebar > .column_padding").removeClass('wk_post');

      	       		}else if( $valore_scroll >= offset_sidebar + distanza_sidebar){
      	       			

      	       		}else if($valore_scroll < distanza_sidebar ){
      	       			$("#sidebar > .column_padding").removeClass('wk_sticky');
      	       			$("#sidebar > .column_padding").removeClass('wk_post').css('top', '0px');
      	       		}
      	       	}
      	      //}

    	}

    	lastScrollTop=$valore_scroll;

    	
    });


    if($('body').hasClass( "has-backskin")){

    
    }else{

    	if($width>999) {
    		banner_top=$(".wk_ads_orizzontale_1").outerHeight();
    		$( '<div class="wk_ads_orizzontale_1_copy" style="height:'+ banner_top + 'px;"></div>' ).insertAfter( ".wk_ads_orizzontale_1" );
    	}
    }


});


function calcolaSideads1_custom(){
	// ricavo altezza sidebar 1
	var dimensione_sidebar=$('#wk_sideads1').innerHeight();
	dimensione_twitter=$('#tweet-link').outerHeight();
	var totalHeight=0;
	// ciclo tutti gli elementi wk_ads presenti nella sidebar
	$('#wk_sideads1').find('.wrapwidg').each(function () {
	    totalHeight += $(this).outerHeight(true) + 20;
	});
	
	var offset_sticky = dimensione_sidebar - totalHeight - dimensione_twitter - 68 - 10;

	return offset_sticky;
}

function calcolaSideads2_custom(){
	// ricavo altezza sidebar 2
	var dimensione_sidebar2=$('#wk_sideads2').innerHeight();
	var totalHeight2=0;
	// ciclo tutti gli elementi wk_ads presenti nella sidebar
	$('#wk_sideads2').find('.wrapwidg').each(function () {
	    totalHeight2 += $(this).outerHeight(true) + 20;
	});
	
	var offset_sticky2 = dimensione_sidebar2 - totalHeight2 - 68 - 10;

	return offset_sticky2;
}



function calcola_sidebar_custom(){
	// ricavo altezza sidebar 2
	var dimensione_sidebar_interna=$('#sidebar').outerHeight();
	var sidebar_height=$('#sidebar .column_padding').outerHeight();

	var offset_sidebar = dimensione_sidebar_interna - sidebar_height;
	return offset_sidebar;

}