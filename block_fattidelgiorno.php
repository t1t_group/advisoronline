<!--- INIZIO FATTI DEL GIORNO -->

<div class="wrapwidg wk_widget" id="fatti-del-giorno">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<a href="/tag/view-tags(primo+piano).action">Fatti del giorno</a>
			<span class="wk_data_giorno">17 febbraio 2020</span>
		</h2>

		<div id="lastContents" class="wk_contenitore_lista">

			<div class="contents_container_all">

				<div class="wk_barra_scroll"></div>
				<div class="wk-owl-carousel owl-carousel wk-owl-fatti-del-giorno">


					<!-- BLOCCO CHE SI RIPETE -->
				  	<div class="wk_item">
				  			<div class="wk_image">
				  				<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
					  				<img alt="marconi-duccio-2020.jpg" src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/54968_marconiducciojpg_highlight.png"> 
					  			</a>
				  			</div>
				  			<div class="wk_articolo">
				  				<span class="wk_categoria">Asset manager</span>
				  				<h4 class="wk_titolo">
				  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Duccio Marconi: “Il CF partner nell’educazione finanziaria”
				  					</a>
				  				</h4>
				  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
				  			</div>
				  		</a>
					</div>
					<!-- FINE BLOCCO CHE SI RIPETE -->

					<div class="wk_item wk_ads">
				  			<div class="wk_image">
				  				<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
					  				<img alt="marconi-duccio-2020.jpg" src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/54968_marconiducciojpg_highlight.png"> 
					  			</a>
				  			</div>
				  			<div class="wk_articolo">
				  				<!-- SE ADS INSERIRE DICITURA CONSIGLIATO DAL PARTNER -->
				  				<span class="wk_categoria">Consigliato dal partner</span>
				  				<h4 class="wk_titolo">
				  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Duccio Marconi: “Il CF partner nell’educazione finanziaria”
				  					</a>
				  				</h4>
				  				<span class="wk_meta"><a href="#">Generali BANK</a></span>
				  			</div>
				  		</a>
					</div>


					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					for($i=0; $i<=7; $i++){
						?>
						  	<div class="wk_item">
  					  			<div class="wk_image">
  					  				<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
  						  				<img alt="marconi-duccio-2020.jpg" src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/54968_marconiducciojpg_highlight.png"> 
  						  			</a>
  					  			</div>
  					  			<div class="wk_articolo">
  					  				<span class="wk_categoria">Asset manager</span>
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Duccio Marconi: “Il CF partner nell’educazione finanziaria”
  					  					</a>
  					  				</h4>
  					  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
  					  			</div>
	  						</div>
						<?php
					}

					?>
				</div>


			</div>

			<div class="customContentListFooter">
				
			</div>

		</div>	
	</div>
</div>


<!--- FINE FATTI DEL GIORNO -->