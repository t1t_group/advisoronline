<!--- INIZIO EDITORIALE -->

<div class="wrapwidg wk_widget" id="editoriale">
	<div class="widget_padding 176380">
		<div class="widget_padding 176393">
			<div id="lastContents176393" class="lastContents-opinioni-opinionisti">
				<div class="contents_container_all"> 


					<!-- codice WEBKOLM -->
					<div class="wk_editoriale wk_contenitore_lista">
						<div class="wk_image_editoriale" style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/francesco.png')">
						</div>
						<div class="wk_articolo">
							<h2>
								<a href="#">L'editoriale</a>
							</h2> 
							<h4>di  Francesco D'Arco </h4> 
							<p>Prima abbiamo parlato di Fase 1. Dopo è stata la volta della Fase 2. E ora qualcuno parla di Fase 3. In mezzo ci sono 215.858 persone che hanno dovuto confrontarsi a livello sanitario con il Coronavirus. Di queste, secondo dati aggiornati al 7 maggio 2020, 29.958 sono decedute, 96.276 sono guarite, il resto sono ancora positive. Non commenterò questi numeri perché dietro a questi numeri ci sono...</p>
							<a href="https://www.advisoronline.it/consulenti-finanziari/reti/55560-troppo-fai-da-te-in-questa-fase-2.action" class="wk_pulsante">Leggi tutto</a>
							<?php include('block_socialsharing.php');?>
						</div>
					</div>
					<!-- fine codice WEBKOLM -->

				</div>
			</div> 
		</div>
	</div>
</div>


<!--- FINE EDITORIALE -->