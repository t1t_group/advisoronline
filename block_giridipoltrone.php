<!--- INIZIO GIRI DI POLTRONE -->


<div class="wrapwidg wk_widget" id="giri-di-poltrone">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<a href="/tag/view-tags(primo+piano).action">Giri di poltrone</a>
		</h2>

		<div id="lastContents" class="wk_contenitore_lista">

			<div class="contents_container_all">

				<div class="wk_barra_scroll"></div>
				<div class="wk-owl-carousel owl-carousel wk-owl-giri-di-poltrone">

					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					for($i=0; $i<=12; $i++){
						?>
						  	<div class="wk_item">
						  		<div class="wk_wrap_image">
	  			  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
	  			  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/54968_marconiducciojpg_highlight.png');" class="wk_image"></a>
	  			  				</div>
  					  			<div class="wk_articolo">
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Credem, la consulenza finanziaria si rafforza in Puglia
  					  					</a>
  					  				</h4>
  					  				<div class="wk_excerpt">A Bari entra Alessandro Latorre, proveniente da Banca Mediolanum...</div>
  					  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
  					  			</div>
	  						</div>
						<?php
					}

					?>
				</div>


			</div>

			<div class="customContentListFooter">
				<a href="" class="wk_pulsante" title="Tutti i giri di poltrone" target="_blank">Scopri tutti i Giri di Poltrone</a>
			</div>

		</div>	
	</div>
</div>


<!--- FINE GIRI DI POLTRONE -->
