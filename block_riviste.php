<!--- INIZIO RIVISTE -->


<div class="wrapwidg wk_widget" id="riviste">
	<div class="widget_padding 176380">
		<div id="lastContents" class="wk_contenitore_lista">
			<div class="contents_container_all wk_contenitore_riviste">

			  	<div class="wk_item_rivista">
			  		<div class="wk_rivista_immagine">
			  			<img src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/02Advisor2018.jpg">
			  		</div>
			  		<div class="wk_riviste_testo">
				  		<h5>ADVISOR</h5>
				  		<p>Il mensile italiano della consulenza finanziaria, edito da gennaio 2011 da OFC.</p>
				  		<a href="#" class="wk_pulsante_rivista">sfoglia ora</a>
				  	</div>
				</div>

				<div class="wk_item_rivista">
	  		  		<div class="wk_rivista_immagine">
	  		  			<img src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/05AP2018.jpg">
	  		  		</div>
	  		  		<div class="wk_riviste_testo">
	  			  		<h5>ADVISOR PRIVATE</h5>
	  			  		<p>Il magazine che raccoglie tutte le news sul mondo del private banking.</p>
	  			  		<a href="#" class="wk_pulsante_rivista wk_private_pulsante">sfoglia ora</a>
	  			  	</div>
				</div>

	  		</div>
		</div>	
	</div>
</div>


<!--- FINE RIVISTE -->
