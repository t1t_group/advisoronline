    <header>
        <!-- BARRA NERA TOP con newsletter e community -->
        <?php include('topbar.php'); ?>

        <!-- SEZIONE CON LOGO PRINCIPALE E LOGHI NETWORK -->
        <?php include('header_logo.php'); ?>

        <!-- MENU PRINCIPALE, MARKUP PRESO DA SITO ADVISORONLINE -->
        <?php include('menu.php'); ?>
    </header>