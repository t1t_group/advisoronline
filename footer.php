	<footer class="wk_footer">
		<div class="wk_wrap">
			<div class=wk_footer_column>
				<a href="/home.action" class="wk_logo_footer">
					<!-- ICONA ADVISOR ONLINE -->
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 781.36 190.84"><style>.wk-cls-1{fill:#706f6f;}.wk-cls-2{fill:#ae0f0a;}</style><path class="wk-cls-1" d="M88.56,77.72l-20-44.86L50.05,77.72ZM62.51,5.64H75L130.51,130.2H112.36L95.85,93.5H43L27.54,130.2H9.32L62.51,5.64Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-1" d="M184.14,22.13H162v92.11h22.69q13.31,0,20.66-1.81a48.64,48.64,0,0,0,12.28-4.62,42,42,0,0,0,9-6.87q13.11-13.29,13.12-33.59,0-19.95-13.48-32.54a43.13,43.13,0,0,0-11.39-7.71,47.68,47.68,0,0,0-12.19-4q-5.76-1-18.53-1ZM144.25,130V6.35H186q25.27,0,40,7.58a57.19,57.19,0,0,1,23.41,22.3,62,62,0,0,1,8.69,32.09A60.71,60.71,0,0,1,239.64,112a59.74,59.74,0,0,1-21,13.39,62.64,62.64,0,0,1-12.86,3.68q-5.85.93-22.34.93Z" transform="translate(-9.32 -4.58)"/><polygon class="wk-cls-1" points="340.29 1.77 357.94 1.77 305.58 126.15 301.56 126.15 248.19 1.77 266.04 1.77 303.29 88.74 340.29 1.77 340.29 1.77"/><polygon class="wk-cls-1" points="371.68 1.77 389.42 1.77 389.42 125.62 371.68 125.62 371.68 1.77 371.68 1.77"/><path class="wk-cls-1" d="M456.89,78.31l-13.48-8.19q-12.68-7.74-18-15.23A28.88,28.88,0,0,1,420,37.65Q420,23,430.16,13.89t26.38-9.13A49.86,49.86,0,0,1,485,13.47V33.59q-13.38-12.87-28.81-12.88-8.69,0-14.27,4T436.32,35a15.82,15.82,0,0,0,4.12,10.42q4.11,4.86,13.23,10.2l13.54,8q22.67,13.53,22.66,34.46,0,14.9-10,24.2T454,131.62a54.48,54.48,0,0,1-33.42-11.28V97.82Q434.91,116,453.79,116a20.88,20.88,0,0,0,13.87-4.64,14.52,14.52,0,0,0,5.54-11.62q0-11.31-16.31-21.45Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" id="o_logo" d="M570.85,21.42q-21.26,0-34.94,13.3t-13.68,34q0,20,13.73,33.34t34.36,13.3q20.73,0,34.59-13.57T618.77,68q0-19.69-13.86-33.12T570.85,21.42Zm-.7-16.84q28.7,0,47.79,18.17T637,68.32q0,27.39-19.27,45.35t-48.59,18q-28,0-46.65-18t-18.64-45q0-27.73,18.78-45.92T570.15,4.58Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-1" d="M682.38,21.42h-5.32v39.1h6.73q13.49,0,18.49-2.31a17.27,17.27,0,0,0,7.8-7,20.15,20.15,0,0,0,2.79-10.6,18.9,18.9,0,0,0-3.1-10.59,16.62,16.62,0,0,0-8.73-6.7q-5.62-1.95-18.66-1.95Zm-23,108.78V6.35h31q18.79,0,29.83,9.31t11,25.18a32.29,32.29,0,0,1-5.41,18.7,32.36,32.36,0,0,1-15.52,11.8A54.39,54.39,0,0,1,721.91,82q5.69,6.75,16,23.5,6.47,10.55,10.37,15.86l6.56,8.87h-21.1l-5.4-8.16c-.18-.3-.54-.8-1.06-1.51l-3.46-4.88-5.49-9.05-5.93-9.66A95.22,95.22,0,0,0,702.31,84.8a38.6,38.6,0,0,0-8.25-6.51q-3.69-2-12.39-2h-4.61v53.9Z" transform="translate(-9.32 -4.58)"/>
					<g id="payoff_logo">
					<path class="wk-cls-2" d="M481.26,172.9q0-10.68,5.95-16.63t16.58-6q10.87,0,16.77,5.85t5.89,16.37q0,7.65-2.57,12.54a18.33,18.33,0,0,1-7.44,7.62,24.58,24.58,0,0,1-12.12,2.72,27.83,27.83,0,0,1-12.21-2.35,18.36,18.36,0,0,1-7.84-7.44A24.7,24.7,0,0,1,481.26,172.9Zm13.48.06c0,4.4.82,7.57,2.46,9.49a9.3,9.3,0,0,0,13.4.06c1.58-1.88,2.38-5.27,2.38-10.14q0-6.17-2.49-9a8.46,8.46,0,0,0-6.73-2.85,8.21,8.21,0,0,0-6.55,2.89Q494.74,166.29,494.74,173Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M532.91,163.08h11.27v5.15a17.05,17.05,0,0,1,5.12-4.51,13.53,13.53,0,0,1,6.31-1.35,10.36,10.36,0,0,1,7.87,3q2.84,3,2.84,9.24v20.08H554.15V177.3c0-2-.37-3.38-1.1-4.21a3.94,3.94,0,0,0-3.09-1.23,4.37,4.37,0,0,0-3.57,1.67c-.92,1.11-1.37,3.1-1.37,6v15.17H532.91Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M574,151.06H586.1v43.62H574Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M594.28,151.06h12.11v8.25H594.28Zm0,12h12.11v31.6H594.28Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M614.13,163.08H625.4v5.15a16.94,16.94,0,0,1,5.12-4.51,13.48,13.48,0,0,1,6.3-1.35,10.33,10.33,0,0,1,7.87,3q2.84,3,2.84,9.24v20.08H635.37V177.3c0-2-.37-3.38-1.1-4.21a4,4,0,0,0-3.1-1.23,4.37,4.37,0,0,0-3.57,1.67q-1.36,1.66-1.37,6v15.17h-12.1Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M689.84,181.88H665.56a7.87,7.87,0,0,0,1.58,4.35,5.72,5.72,0,0,0,4.58,2.05,6.92,6.92,0,0,0,3.39-.89,7.8,7.8,0,0,0,2.11-2l11.93,1.11a17,17,0,0,1-6.6,6.82q-3.87,2.07-11.1,2.07c-4.18,0-7.48-.59-9.87-1.77a14.52,14.52,0,0,1-6-5.62,17,17,0,0,1-2.36-9.06A15.91,15.91,0,0,1,658,167q4.75-4.57,13.1-4.58,6.79,0,10.72,2a13.89,13.89,0,0,1,6,6,21.74,21.74,0,0,1,2.06,10.15Zm-12.32-5.8q-.36-3.51-1.89-5a5.92,5.92,0,0,0-8.64.77,8.43,8.43,0,0,0-1.4,4.25Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M695.43,182.54h12.94v12.14H695.43Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M716.14,151.06h12.1v8.25h-12.1Zm0,12h12.1v31.6h-12.1Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-2" d="M750.59,151.06v12h6.66V172h-6.66v11.19a6.13,6.13,0,0,0,.38,2.67,2.21,2.21,0,0,0,2.08,1,13,13,0,0,0,3.75-.77l.9,8.36a39.74,39.74,0,0,1-8.39,1c-3,0-5.24-.38-6.67-1.16a7.05,7.05,0,0,1-3.17-3.52q-1-2.36-1-7.65V172H734v-8.87h4.47v-5.8Z" transform="translate(-9.32 -4.58)"/><path class="wk-cls-1" d="M778.12,105.32c-8,0-12.59,5-12.59,12.43s4.59,12.45,12.59,12.45,12.56-5,12.56-12.45-4.65-12.43-12.56-12.43m0,23.21c-6.26,0-10.59-3.73-10.59-10.78S771.86,107,778.12,107s10.5,3.73,10.5,10.71-4.32,10.78-10.5,10.78" transform="translate(-9.32 -4.58)"/><path class="wk-cls-1" d="M783.1,115.29c0-2.39-1.6-3.92-4.59-3.92h-5.19v12.24h2.27v-4.06h2.72l2.19,4.06H783l-2.38-4.39a3.77,3.77,0,0,0,2.46-3.93m-4.92,2.33h-2.59v-4.33h2.92a2,2,0,0,1,2.26,2.14c0,1.66-.94,2.19-2.59,2.19" transform="translate(-9.32 -4.58)"/>
					</g>
					</svg>
				</a>
			</div>
			<div class="wk_footer_column">
				<ul>
					<li><a href="/chisiamo.action">Chi siamo</a></li>
					<li><a href="/newsletter.action" target="_blank">Iscriviti alla newsletter</a></li>
					<li><a href="/privacy.action">Privacy policy</a></li>
					<li><a href="/terminiduso.action">Termini d'uso</a></li>
					<li><a href="/contatti.action">Contatti</a></li>
				</ul>
			</div>
			<div class="wk_footer_column nomobile">
				<ul>
					<li><a href="/user/login.action">Iscriviti alla businness community</a></li>
					<li><a href="https://www.myadvisoronline.it/" target="_blank">Abbonati alle riviste</a></li>
					<li><a href="/consulenti-finanziari.action">Consulenti finanziari</a></li>
					<li><a href="/private-banker.action">Private banker</a></li>
					<li><a href="/asset-manager.action">Asset Manager</a></li>
				</ul>
			</div>
			<div class="wk_footer_column nomobile">
				<ul>
					<li><a href="/assicurazioni-e-banche.action">Assicurazioni e Banche</a></li>
					<li><a href="/strumenti-finanziari.action">Strumenti finanziari</a></li>
					<li><a href="/associazioni.action">Associazioni</a></li>
					<li><a href="/normative-e-fisco.action">Normative e Fisco</a></li>
					<li><a href="/tag/view-tags(primo+piano).action">Primo piano</a></li>
				</ul>
			</div>
			<div class="wk_footer_column ultima_colonna nomobile">
				<ul>
					<li><a href="/tag/view-tags(news+generica).action">I fatti del giorno</a></li>
					<li><a href="/tag/view-tags(opinioni+opinionisti).action">Opinioni e opinionisti</a></li>
					<li><a href="/strumenti-finanziari.action">Prodotti</a></li>
					<li><a href="/consulenti-finanziari/carriere-e-professione.action">Giri di poltrone</a></li>
					<li><a href="https://advisorplay.advisoronline.it/" target="_blank">Advisor Play</a></li>
				</ul>
			</div>
			<div class="wk_footer_column onlymobile">
				<select name="wk_footer_menumobile" class="wk_footer_menumobile" id="wk_footer_menu">
					<option value="/sitemap.action">Mappa del sito...</option>
					<option value="/chisiamo.action">Chi siamo</option>
					<option value="/chisiamo.action">Iscrizioni alle newsletter</option>
					<option value="/privacy.action">Privacy Policy</option>
					<option value="/terminiduso.action">Termini d'uso</option>
					<option value="/contatti.action">Contatti</option>
					<option value="/user/login.action">Iscriviti alla businness community</option>
					<option value="https://www.myadvisoronline.it/">Abbonati alle riviste</option>
					<option value="/consulenti-finanziari.action">Consulenti finanziari</option>
					<option value="/private-banker.action">Private banker</option>
					<option value="/asset-manager.action">Asset Manager</option>
					<option value="/assicurazioni-e-banche.action">Assicurazioni e Banche</option>
					<option value="/strumenti-finanziari.action">Strumenti finanziari</option>
					<option value="/associazioni.action">Associazioni</option>
					<option value="/normative-e-fisco.action">Normative e Fisco</option>
					<option value="/tag/view-tags(primo+piano).action">Primo piano</option>
					<option value="/tag/view-tags(news+generica).action">I fatti del giorno</option>
					<option value="/tag/view-tags(opinioni+opinionisti).action">Opinioni e opinionisti</option>
					<option value="/strumenti-finanziari.action">Prodotti</option>
					<option value="/consulenti-finanziari/carriere-e-professione.action">Giri di poltrone</option>
					<option value="https://advisorplay.advisoronline.it/">Advisor Play</option>
				</select>
			</div>
			<div class="wk_credits">P. IVA IT 07114400968 - <a href="http://www.o-fc.it/" target="_blank">Powered by OFC</a> - <a href="https://www.reply.com/tamtamy-reply/it/" target="_blank">TamTamy platform by Reply</a> - <a href="https://webkolm.com/" target="_blank">Designed by Webkolm</a> - <a>Privacy policy</a> - <a>Cookie policy</a></div>
		</div>
	</footer>

	<div class="wk_modale wk_c2a_abbonati">
		<div class="wk_modale_wrap">
			<i class="fal fa-times"></i>
			<div class="slider">
				<ul class="slides">
					<li style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/02Advisor2018.jpg');">
					</li>
					<li style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/www/05AP2018.jpg');">
					</li>
				</ul>
			</div>
			<h2>Abbonati ad ADVISOR</h2>
			<div class="wk_testo_modale">
				Il Newsmagazine della consulenza finanziaria.<br/>
				Tutte le notizie delle reti: strutture, giri di poltrone e gli approfondimenti normativi utili alla professione
			</div>
			<a href="https://www.myadvisoronline.it/?utm_source=advisoronline&utm_medium=c2a&utm_campaign=myAdvisor_from_advisoronline" class="wk_pulsante">Clicca qui per abbonarti</a>
		</div>
	</div>

	<div class="wk_modale wk_c2a_newsletter">
		
		<div class="wk_modale_wrap">
			<i class="fal fa-times"></i>
			<div class="slider">
				<ul class="slides">
					<li style="background-image: url('https://www.advisoronline.it//pub/thumb/www/TopContent2014.jpg');">
					</li>
					<li style="background-image: url('https://www.advisoronline.it//pub/thumb/www/Weekly2014.jpg');">
					</li>
				</ul>
			</div>
			<h2>Iscriviti alla nostra newsletter</h2>
			<div class="wk_testo_modale">
				Scegli di rimanere aggiornato solo sugli argomenti che più ti interessano. Il sistema ADVISOR ti da la possibiltà di avere un'informazione personalizzata in base alla tua professione e ai tuoi interessi.
				<h4>Advisor Today</h4>
				Le top news del giorno, ogni mattina da lunedì al venerdì
				<h4>AdvisorWeekly</h4>
				I main topic della consulenza finanziaria, il sabato
				<h4>TopContent Professional</h4>
				News dal mondo dei promotori finanziari, 4 a settimana
				<h4>Follow Blog Professional</h4>
				I suggerimenti dei nostri blogger sulla professione, la domenica
			</div>
			<a href="https://www.advisoronline.it/newsletter.action" class="wk_pulsante">Iscriviti ora</a>
		</div>
		
	</div>

	<div class="wk_modale wk_c2a_professional">
		
		<div class="wk_modale_wrap">
			<i class="fal fa-times"></i>
			<div class="slider">
				<ul class="slides">
					<li style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/55638_highlight.png');">
					</li>
				</ul>
			</div>
			<h2>Entra in Advisor Professional!</h2>
			<div class="wk_testo_modale">
				AdvisorProfessional è la prima community interamente dedicata ai consulenti finanziari con oltre 12000 professionisti già iscritti. Video prodotti, video interviste, l'agenda degli eventi, gli outlook di mercato, le novità per la professione e l'assistenza di esperti legali fiscali: questi sono alcuni dei più importanti contenuti disponibili gratuitamente per i promotori finanziari registrati.
			</div>
			<a href="https://professional.advisoronline.it/user/login.action" class="wk_pulsante">Registrati ora</a>
		</div>
		
	</div>
    

    <?php $random_asset=rand(0,1000); ?>
    <!-- CSS -->
    <link rel="stylesheet" href="css/stile.css?v=<?= $random_asset ?>">

    <!-- JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="/js/min/sito.min.js?v=<?= $random_asset ?>"></script>
    