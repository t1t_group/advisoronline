<div class="wrapwidg " id="macro_category">
	<div class="widget_padding 62558 macro_category">
		<div id="lastContents62558" class="lastContents-macro_category">
			<div class="contents_container_all">

				<?php

				// CICLO PER CONTENUTI RIEMPITIVI

				for($i=0; $i<=9; $i++){
					?>
					  	<div class="wk_item">
					  		<div class="wk_wrap_image">
  			  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
  			  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/54968_marconiducciojpg_highlight.png');" class="wk_image"></a>
  			  				</div>
				  			<div class="wk_articolo">
				  				<h4 class="wk_titolo">
				  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Fideuram ISPB e l'elogio dei private banker
				  					</a>
				  				</h4>
				  				<div class="wk_excerpt">Marzo si chiude con masse pari a 223 miliardi di euro, in flessione rispetto a fine 2019, ma in linea con il primo trimestre 2019. Gli effetti negativi dei mercati sono stati compensati dal risultato di raccolta netta conseguito dalle reti.</div>
				  				<span class="wk_meta">27/03/2020 | <a href="#">Daniele Riosa</a></span>
				  				<?php include('block_socialsharing.php');?>
				  			</div>
  						</div>
					<?php
				}

				?>


			</div>
			<div class="pager_wrap">
				<div>
					<div class="wrappager">
						<ul class="pager">
							<li>
								<a class="on" href="javascript:void(0);" onclick="return false;">
									1
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="return linkAction('lastContents62558', '/content/home-container(62558)-template(custom)-showCategoryFilter(false)-target(all)-firstResult(13)-numberOfResult(13)-visibleOrder(publicationDate,hottest,activityDate)-tag(|fondi+comuni OR assicurativi+previdenziali OR real+estate OR etf OR certificates OR hedge+real+estate OR alternativi|)-strBehaviorEq(tag)-orderByDesc(publicationDate)-permission(|PUBLIC|)-excludeTags(||).action', ' ', null, true);">2</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="return linkAction('lastContents62558', '/content/home-container(62558)-template(custom)-showCategoryFilter(false)-target(all)-firstResult(26)-numberOfResult(13)-visibleOrder(publicationDate,hottest,activityDate)-tag(|fondi+comuni OR assicurativi+previdenziali OR real+estate OR etf OR certificates OR hedge+real+estate OR alternativi|)-strBehaviorEq(tag)-orderByDesc(publicationDate)-permission(|PUBLIC|)-excludeTags(||).action', ' ', null, true);">3</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="return linkAction('lastContents62558', '/content/home-container(62558)-template(custom)-showCategoryFilter(false)-target(all)-firstResult(39)-numberOfResult(13)-visibleOrder(publicationDate,hottest,activityDate)-tag(|fondi+comuni OR assicurativi+previdenziali OR real+estate OR etf OR certificates OR hedge+real+estate OR alternativi|)-strBehaviorEq(tag)-orderByDesc(publicationDate)-permission(|PUBLIC|)-excludeTags(||).action', ' ', null, true);">4
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="return linkAction('lastContents62558', '/content/home-container(62558)-template(custom)-showCategoryFilter(false)-target(all)-firstResult(52)-numberOfResult(13)-visibleOrder(publicationDate,hottest,activityDate)-tag(|fondi+comuni OR assicurativi+previdenziali OR real+estate OR etf OR certificates OR hedge+real+estate OR alternativi|)-strBehaviorEq(tag)-orderByDesc(publicationDate)-permission(|PUBLIC|)-excludeTags(||).action', ' ', null, true);">5
								</a>
							</li>
							<li class="next" id="next_lastContents62558">
								<a href="javascript:void(0);" onclick="return linkAction('lastContents62558', '/content/home-container(62558)-template(custom)-showCategoryFilter(false)-target(all)-firstResult(13)-numberOfResult(13)-visibleOrder(publicationDate,hottest,activityDate)-tag(|fondi+comuni OR assicurativi+previdenziali OR real+estate OR etf OR certificates OR hedge+real+estate OR alternativi|)-strBehaviorEq(tag)-orderByDesc(publicationDate)-permission(|PUBLIC|)-excludeTags(||).action', ' ', null, true);">»</a>
							</li>
						</ul>
					</div>
				</div>
			</div>	 
		</div>	
	</div>
</div>