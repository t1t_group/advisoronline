/**
 *  REQUIRES
 **/

var gulp = require('gulp');
var repoWatch = require('gulp-repository-watch');
var gutil = require('gulp-util');
var git = require('gulp-git');
var runSequence = require('run-sequence');
var fs = require('fs');

// debugger
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var inject = require('gulp-inject');
var prefix = require('gulp-autoprefixer');
var gulpif = require('gulp-if');
var prompt = require('gulp-prompt');
var rsync  = require('gulp-rsync');
var argv   = require('minimist')(process.argv);

var config = require('./settings.json');

var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};



/**************** GULP TASK MARCO TONET *************************/


gulp.task('sass', function() {
  return gulp.src('./scss/**/*.scss')
      .pipe(plumber(plumberErrorHandler))
      .pipe(sass({errLogToConsole: true}))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(prefix(
        "last 1 version", "> 1%", "ie 8", "ie 7"
        ))
      .pipe(gulp.dest('./css'));


})


gulp.task('script', function(){
    return gulp.src(['./js/*'])
        .pipe(concat('sito.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/min/.'));


});



gulp.task('webkolm', function () {

    gulp.watch('./js/sito.js',['script']);
    var css_watch = gulp.watch('./scss/**/*.scss',['sass']);
    
    css_watch.on('change',function(f){

        rsyncConf = {
            progress: true,
            incremental: true,
            relative: true,
            emptyDirectories: true,
            recursive: true,
            port: "22",
            clean: true,
        };

        rsyncConf.hostname = "139.59.128.181"; // hostname
        rsyncConf.username = "artidelmovimento"; // ssh username
        rsyncConf.destination = "/srv/users/artidelmovimento/apps/advisoronline/public/"; // path where uploaded files go

        return gulp.src('./').pipe(rsync(rsyncConf));
        
    });

});



/**************** FINE GULP TASK MARCO TONET *************************/

