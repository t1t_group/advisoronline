<?php 

	$url_news="inserire_qui_l_url_della_news";
	$title_news="inserire_qui_il_titolo_della_news";
	$excerpt_news="inserire_qui_l_estratto_della_news";
?>
<ul class="wk_social_sharing">
	<li>
		<a title="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $url_news ?>&title=<?= $title_news ?>&summary=<?= $excerpt_news ?>&source=">
			<span class="icon-linkedin"></span>
		</a>
	</li>
	<li>
		<a title="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= $url_news ?>">
			<span class="icon-facebook"></span>
		</a>
	</li>
	<li>
		<a title="twitter" href="https://twitter.com/intent/tweet?url=<?= $url_news; ?>&text=<?= $title_news; ?>">

			<span class="icon-twitter"></span>
		</a>
	</li>
	<li>
		<a title="whatsupp" href="https://api.whatsapp.com/send?text=<?= $url_news ?>" data-action="share/whatsapp/share">
			<span class="icon-whatsapp"></span>
		</a>
	</li>
	<li>
		<a title="mail" href="mailto:?&subject=<?= $title_news ?>&body=<?= $url_news ?>">
			<span class="icon-mail"></span>
		</a>
	</li>
	<li>
		<a class="wk_social_expand">
			<span class="icon-share"></span>
		</a>
	</li>
</ul>