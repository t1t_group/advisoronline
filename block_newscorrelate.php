<!--- INIZIO NEWS CORRELATE -->

<div class="wrapwidg wk_widget" id="news-correlate">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<!-- DEFINIRE LINK A NEWS PIU LETTE -->
			<a href="#">NOTIZIE CORRELATE</a>
		</h2>

		<div id="lastContents2" class="wk_contenitore_lista">

			<div class="contents_container_all">

					<!-- BLOCCO CHE SI RIPETE -->
				  	<div class="wk_item" onclick="location.href='/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action';">
				  			<div class="wk_articolo">
				  				<span class="wk_categoria">Strumenti finanziari</span>
				  				<h4 class="wk_titolo">
				  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
				  						Coronavirus, l’impatto sul settore del lusso.
				  					</a>
				  				</h4>
				  				<span class="wk_meta">27/03/2020 | <a href="#">Redazione</a></span>
				  			</div>
					</div>
					<!-- FINE BLOCCO CHE SI RIPETE -->

					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					for($i=0; $i<=2; $i++){
						?>
						  	<div class="wk_item"  onclick="location.href='/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action';">
						  			<div class="wk_articolo">
						  				<span class="wk_categoria">Strumenti finanziari</span>
						  				<h4 class="wk_titolo">
						  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">
						  						Certificates sugli scudi, cash collect i più gettonati.
						  					</a>
						  				</h4>
						  				<span class="wk_meta">27/03/2020 | Redazione</span>
						  			</div>
						  		</a>
							</div>
						<?php
					}

					?>

			</div>

			<div class="customContentListFooter">
				
				<a href="" class="wk_pulsante">Tutte le correlate</a>

			</div>

		</div>	
	</div>
</div>


<!--- FINE NEWS CORRELATE -->