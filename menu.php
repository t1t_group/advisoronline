<!-- MENU PRINCIPALE -->
<div class="wk_menu">
	<div class="hamburger-menu">
		<span></span>
	</div>
	<a href="/home.action" class="logo_o">
		<!-- ICONA O del logo ADVISOR -->
		<svg x="0px" y="0px" viewBox="0 0 500 500" xml:space="preserve">
		<path  d="M248.8,17.2c70.2,0,128.6,22.2,175.2,66.6c46.7,44.4,70,100.1,70,167c0,66.9-23.6,122.3-70.7,166.2
			c-47.1,43.9-106.4,65.8-178.1,65.8c-68.4,0-125.4-21.9-171-65.8C28.7,373.2,5.9,318.2,5.9,252.1c0-67.8,22.9-123.9,68.9-168.3
			C120.7,39.4,178.7,17.2,248.8,17.2L248.8,17.2L248.8,17.2z M251.5,78.9c-52,0-94.6,16.2-128.1,48.7c-33.4,32.5-50.2,74-50.2,124.5
			c0,48.9,16.8,89.7,50.3,122.2c33.5,32.5,75.5,48.8,126,48.8c50.6,0,92.9-16.6,126.8-49.7c33.9-33.2,50.8-74.4,50.8-123.8
			c0-48.1-16.9-88.6-50.8-121.4C342.4,95.3,300.8,78.9,251.5,78.9L251.5,78.9L251.5,78.9z"/>
		</svg>
	</a>

		<div class="wk_wrap">
		<ul id="globalNavBar">

			<li class="active">
				<a href="/consulenti-finanziari.action" class="expandable">Consulenti Finanziari</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/consulenti-finanziari/carriere-e-professione.action" class="expandable">Carriere e Professione</a>
							<div class="dropdown level_two"  >
								<ul>
									<li class="drop-voice"><a href="/consulenti-finanziari/carriere-e-professione/giri-di-poltrone.action">Giri di poltrone</a></li>
								</ul>
							</div>
						</li>
						<li class="drop-voice"><a href="/consulenti-finanziari/numeri-delle-reti.action">Numeri delle Reti</a></li>
						<li class="drop-voice"><a href="/consulenti-finanziari/reti.action" class="expandable">Reti</a>
							<div class="dropdown level_two"  >
								<ul>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/allianz.action">Allianz Bank FA</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/azimut.action">Azimut</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/generali.action">Banca Generali</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/bnp-paribas-life-banker.action">BNL - BNP Paribas Life Banker</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/chebanca.action">CheBanca!</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/credem.action">Credem</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/deutsche-bank-fa.action">Deutsche Bank FA</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/fideuram.action">Fideuram</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/fineco.action">Fineco</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/iwbank.action">IWBank</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/mediolanum.action">Mediolanum</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/widiba.action">Widiba</a></li>
									<li class="drop-voice"><a href="/consulenti-finanziari/reti/altro.action">Altre</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</li>
			<li class="">
				<a href="/asset-manager.action" class="expandable">Asset Manager</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/asset-manager/carriere-e-professione.action">Carriere e Professione</a>
						</li>
						<li class="drop-voice"><a href="/asset-manager/numeri-delle-sgr.action">Numeri delle SGR</a></li>
						<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio.action" class="expandable">Società di Gestione del Risparmio</a>
							<div class="dropdown level_two" style="">
								<ul>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/aberdeen-am.action">Aberdeen AM</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/acomea.action">AcomeA</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/allianz-global-investors.action">Allianz Global Investors</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/amundi.action">Amundi</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/anima.action">Anima</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/arca.action">Arca</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/axa-investment-managers.action">AXA Investment Managers</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/blackrock.action">BlackRock</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/bnp-paribas-investment-partners.action">BNP Paribas Investment Partners</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/fidelity.action">Fidelity International</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/franklin-templeton-investments.action">Franklin Templeton Investments</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/jp-morgan-am.action">JP Morgan AM</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/generali.action">Generali</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/intesa-sanpaolo.action">Intesa Sanpaolo</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/invesco.action">Invesco</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/morgan-stanley.action">Morgan Stanley</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/natixis.action">Natixis</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/ing-investment-management.action">NN Investment Management</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/pictet.action">Pictet</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/schroders.action">Schroders</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio/vontobel.action">Vontobel</a></li>
									<li class="drop-voice"><a href="/asset-manager/societa-di-gestione-del-risparmio.action">Altre</a></li>
								</ul>
							</div>
						</li>
						<li class="drop-voice"><a href="/asset-manager/gestori-e-mercati-finanziari.action">Gestori e Mercati Finanziari</a></li>
						<li class="drop-voice"><a href="/strumenti-finanziari/fondi-comuni-di-investimento-sicav.action">Fondi Comuni di Investimento/Sicav</a></li>
						
					</ul>
				</div>
			</li>
			<li class="">
				<a href="/private-banker.action">Private Banker</a>
				
			</li>
			<li class="">
				<a href="/strumenti-finanziari.action" class="expandable">Strumenti Finanziari</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/strumenti-finanziari/fondi-comuni-di-investimento-sicav.action">Fondi comuni di investimento SICAV</a></li>
						<li class="drop-voice"><a href="/strumenti-finanziari/polizze-e-fondi-pensione.action">Polizze e Fondi pensione</a></li>
						<li class="drop-voice"><a href="/strumenti-finanziari/etf.action">ETF</a></li>
						<li class="drop-voice"><a href="/strumenti-finanziari/certificati.action">Certificati</a></li>
					</ul>
				</div>
			</li>
			<li class="">
				<a href="/associazioni.action" class="expandable">Associazioni</a>
				<div class="dropdown level_one"  >
					<ul>

						<li class="drop-voice"><a href="/associazioni/abi.action">ABI</a></li>
						<li class="drop-voice"><a href="/associazioni/acepi.action">ACEPI</a></li>
						<li class="drop-voice"><a href="/associazioni/aipb.action">AIPB</a></li>
						<li class="drop-voice"><a href="/associazioni/anasf.action">Anasf</a></li>
						<li class="drop-voice"><a href="/associazioni/ania.action">Ania</a></li>
						<li class="drop-voice"><a href="/associazioni/ascosim.action">Ascofind</a></li>
						<li class="drop-voice"><a href="/associazioni/assogestioni.action">Assogestioni</a></li>
						<li class="drop-voice"><a href="/associazioni/assoreti.action">Assoreti</a></li>
						<li class="drop-voice"><a href="/associazioni/efama.action">Efama</a></li>
						<li class="drop-voice"><a href="/associazioni/efpa.action">EFPA</a></li>
						<li class="drop-voice"><a href="/associazioni/fecif.action">Fecif</a></li>
						<li class="drop-voice"><a href="/associazioni/nafop.action">Nafop</a></li>
					</ul>
				</div>
			</li>
			<li class="">
				<a class="expandable">Altro</a>
				<div class="dropdown level_one"  >
					<ul>
						<!-- RICHIESTA DI FRANCESCO D'ARCO -->
						<!-- dovrà unire i contenuti di
queste tre voci: Albo Promotore Finanziario APF; Albo Consulenti finanziari; OAM Mediatori creditizi -->
						<li class="drop-voice"><a href="/albo/albo-consulenti-finanziari.action">Albo Consulenti Finanziari</a></li>
						<li class="drop-voice"><a href="/albo/consob.action">CONSOB</a></li>
						<li class="drop-voice"><a href="/assicurazioni-e-banche.action">Assicurazioni e banche</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco.action">Normative e fisco</a></li>
					</ul>
				</div>
			</li>

			<li class="network_action n_news" data-modale="wk_c2a_newsletter">
				<a>
				    <!-- ICONA NEWSLETTER -->
				    <svg data-name="Livello 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 62.1"><path class="cls-1" d="M88.42,19H11.58A6.59,6.59,0,0,0,5,25.53V74.47a6.59,6.59,0,0,0,6.58,6.58H88.42A6.59,6.59,0,0,0,95,74.47V25.53A6.59,6.59,0,0,0,88.42,19Zm3,6.58V74.47a3.05,3.05,0,0,1-.05.52L65.28,50,91.35,25A2.84,2.84,0,0,1,91.4,25.53Zm-3-3,.27,0L50,59.67,11.31,22.58l.27,0ZM8.65,75a3.05,3.05,0,0,1-.05-.52V25.53A2.84,2.84,0,0,1,8.65,25L34.72,50Zm2.93,2.46a2.06,2.06,0,0,1-.26,0l26-24.93,11.43,11a1.81,1.81,0,0,0,2.5,0l11.43-11,26,24.93a2.06,2.06,0,0,1-.26,0Z" transform="translate(-5 -18.95)"/></svg>
				    <span>Iscriviti alla newsletter</span>
				</a>
			</li>
			<li class="network_action n_comm" data-modale="wk_c2a_professional">
				<a>
				    <!-- ICONA COMMUNITY -->
				    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.12 60.61"><path class="cls-1" d="M63.48,80.22H5.94V72.15A5.7,5.7,0,0,1,8,67.71,92.81,92.81,0,0,1,24.88,56.89a.53.53,0,0,0,.3-.47V50.9a13.15,13.15,0,0,1-1.64-4.44c-.62-.06-1.45-.89-2.33-4.06-1.22-4.29.07-4.93,1.16-4.82a10.85,10.85,0,0,1-.47-1.78h0a12.49,12.49,0,0,1,0-5.64,12.23,12.23,0,0,1,3.22-5.84,15.73,15.73,0,0,1,3.05-2.53,12.19,12.19,0,0,1,3-1.52h0a10.1,10.1,0,0,1,2.66-.51,11.06,11.06,0,0,1,6.77,1.43,8.4,8.4,0,0,1,3.36,3.06s5.47.39,3.62,11.48a12,12,0,0,1-.47,1.78c1.09-.11,2.38.53,1.16,4.82-.89,3.13-1.71,4-2.33,4.06a13,13,0,0,1-1.64,4.44v5.64a.52.52,0,0,0,.29.47A93.37,93.37,0,0,1,61.42,67.78a5.72,5.72,0,0,1,2.06,4.44ZM92.15,68.77a85.33,85.33,0,0,0-15.5-10,.48.48,0,0,1-.27-.44V53.16a12,12,0,0,0,1.46-4c.58,0,1.34-.89,2.15-3.74,1.12-4-.07-4.54-1.07-4.44a10.68,10.68,0,0,0,.4-1.67C81.1,29,76,28.69,76,28.69a7.74,7.74,0,0,0-3-2.83,10.15,10.15,0,0,0-6.22-1.31,9.07,9.07,0,0,0-2.47.47h0a10.71,10.71,0,0,0-2.74,1.39,15.28,15.28,0,0,0-2.81,2.32,11.46,11.46,0,0,0-3,5.33,11.75,11.75,0,0,0,0,5.21h0a9.93,9.93,0,0,0,.44,1.62c-1-.09-2.2.49-1.08,4.45.83,2.89,1.59,3.69,2.17,3.78a12.1,12.1,0,0,0,1.51,4.12V58.4a.47.47,0,0,1-.27.43l-.78.38a.48.48,0,0,0-.2.66.52.52,0,0,0,.15.18c2,1.39,4.19,3,6.29,4.68a9.67,9.67,0,0,1,3.55,7.52v8.06H94.06V72.83A5.27,5.27,0,0,0,92.15,68.77Z" transform="translate(-5.94 -19.69)"/></svg>
				    <span>Si un consulente finanziario?</span>
				</a>
			</li>
			<li class="network_action n_act"  data-modale="wk_c2a_abbonati">
				<a>
				    <!-- ICONA MAGAZINE -->
				    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.13 74.28"><path class="cls-1" d="M30.53,87.14l50.54-1.06-1.88-5.42C72.41,83,55.38,85.88,30.53,87.14Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M37.69,84.72c14.7-1.44,34.73-4.2,43.38-8.12l-3-7.91C68.66,75.42,50.85,81.14,37.69,84.72Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M65.08,12.86S62,20.34,50,27.3c-8.2,4.76-21.27,8-21.27,8l-9.8,50.79s22.6-6.63,39.71-18.31c16.24-11.08,18.56-17,18.56-17ZM54,48.11c-8,4.65-19,8.16-25,9.86l3.66-19c4.49-1.27,13.26-4,19.59-7.7a52.06,52.06,0,0,0,11.08-8.44l4.67,14.61A67.51,67.51,0,0,1,54,48.11Z" transform="translate(-18.93 -12.86)"/><path class="cls-1" d="M60.38,70.33c-5.6,3.82-15.29,8.6-21.05,11.3C51.42,78.15,72,70.54,79,63.91l-2.8-6.64C73.36,60.38,68.53,64.76,60.38,70.33Z" transform="translate(-18.93 -12.86)"/></svg>
				    <span>Abbonati alle riviste</span>
				</a>
			</li>

			<?php include('header_network.php'); ?>


			<!--
			<li class="">
				<a href="/albo.action" class="expandable">Albo</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/albo/albo-promotore-finanziario-apf.action">Albo Promotore Finanziario APF</a></li>
						<li class="drop-voice"><a href="/albo/albo-consulenti-finanziari.action">Albo Consulenti Finanziari</a></li>
						<li class="drop-voice"><a href="/albo/oam-mediatori-creditizi.action">OAM Mediatori Creditizi</a></li>
						<li class="drop-voice"><a href="/albo/consob.action">Consob</a></li>
					</ul>
				</div>
			</li>
			<li class="">
				<a href="/assicurazioni-e-banche.action" class="expandable">Assicurazioni e Banche</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/assicurazioni-e-banche/carriere-e-professione.action">Carriere e Professione</a>
							<div class="dropdown level_two" style="">
								<ul>
									<li class="drop-voice"><a href="/assicurazioni-e-banche/carriere-e-professione/giri-di-poltrone.action">Giri di poltrone</a></li>
									<li class="drop-voice"><a href="/assicurazioni-e-banche/carriere-e-professione/professionisti-in-evidenza.action">Professionisti in evidenza</a></li>
								</ul>
							</div>
						</li>
						<li class="drop-voice"><a href="/assicurazioni-e-banche/numeri-assicurazioni-e-banche.action">Numeri delle Assicurazioni e delle Banche</a></li>
						<li class="drop-voice"><a href="/assicurazioni-e-banche/compagnie-assicurative.action">Compagnie Assicurative</a></li>
						<li class="drop-voice"><a href="/assicurazioni-e-banche/banche.action">Banche</a></li>
					</ul>
				</div>
			</li>
			
			
			<li>
				<a href="/normative-e-fisco.action" class="expandable">Normative e Fisco</a>
				<div class="dropdown level_one"  >
					<ul>
						<li class="drop-voice"><a href="/normative-e-fisco/mifid.action">Mifid</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/ucits.action">Ucits</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/aifmd.action">Aifmd</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/imd2.action">IMD2</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/prips.action">Prips</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/fisco.action">Fisco</a></li>
						<li class="drop-voice"><a href="/normative-e-fisco/banca-d-italia.action">Banca d'Italia</a></li>
						<li class="drop-voice"><a href="/albo/consob.action">Consob</a></li>
					</ul>
				</div>
			</li>

		-->
		 </ul>
	 <!-- FINE MENU PRINCIPALE -->

	 <!-- ICONE FUNZIONALI -->
	 <ul class="wk_menu_icons">
	 	<li>
	 		<a href="">
	 			<!-- ICONA FACEBOOK -->
	 			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.37 43.41"><path class="cls-1" d="M34.9,18.66H27.31V12.15a3,3,0,0,1,3-2.72h4.9V3.19H28.13c-3.25,0-6,.55-7.6,2.17a6.87,6.87,0,0,0-2.7,5.7v7.87h-3v6h3V46.61H27.3V24.9h6.78C34.35,24.9,34.9,21.92,34.9,18.66Z" transform="translate(-14.81 -3.19)"/></svg>
	 		</a>
	 	</li>
	 	<li>
	 		<a href="">
	 			<!-- ICONA LINKEDIN -->
	 			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.06 43.46"><path d="M18.29,26.08V23.32a22.21,22.21,0,0,1-.09-2.49,20.17,20.17,0,0,1-.09-2.33A18.61,18.61,0,0,1,18,16.35l8.18.09.43,4.13.17.08A6.8,6.8,0,0,1,28.1,19a12.52,12.52,0,0,1,2-1.55,10.86,10.86,0,0,1,2.67-1.29,19.49,19.49,0,0,1,3.44-.52,13.64,13.64,0,0,1,4.3.86,8.73,8.73,0,0,1,3.45,2.41A13.2,13.2,0,0,1,46.26,23,19.48,19.48,0,0,1,47,28.83V46.65l-9.46.08V29.86a13.59,13.59,0,0,0-.26-2.67,7.41,7.41,0,0,0-.78-2.06,5.16,5.16,0,0,0-1.46-1.29,3.68,3.68,0,0,0-2.24-.43,2.88,2.88,0,0,0-1.72.26,10.27,10.27,0,0,0-1.46.77,3.86,3.86,0,0,0-.95,1.12A10.12,10.12,0,0,0,28,26.85c-.08.26-.08.43-.17.52a1.21,1.21,0,0,0-.09.6,1.8,1.8,0,0,0-.08.6V46.65l-9.38.08V26.08ZM3.23,46.65V16.44l9.38-.09V46.73ZM13,8a4.44,4.44,0,0,1-.34,1.81,5,5,0,0,1-1,1.55,4.65,4.65,0,0,1-1.55,1,4.29,4.29,0,0,1-2.15.34,3.68,3.68,0,0,1-2-.34,4.75,4.75,0,0,1-1.55-1,5.12,5.12,0,0,1-1-1.55A4.44,4.44,0,0,1,3,8a4.57,4.57,0,0,1,.34-1.89,5.86,5.86,0,0,1,1-1.46A5.4,5.4,0,0,1,6,3.61a11.44,11.44,0,0,1,2-.34,13.66,13.66,0,0,1,2.15.34,4.56,4.56,0,0,1,1.46,1,5.65,5.65,0,0,1,1,1.46A4.57,4.57,0,0,1,13,8Z" transform="translate(-2.97 -3.27)"/></svg>
	 		</a>
	 	</li>
	 	<li>
	 		<a href="">
	 			<!-- ICONA TWITTER -->
	 			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.5 43.61"><path class="cls-1" d="M45.08,17.51C47,15.33,48.5,15,49.75,15c-1.87,0-3.74-1.86-5.6-1.86,2.18-1.25,3.73-5.3,4.35-7.79-1.86,1.25-4,1.25-6.22,1.87-1.87-2.18-4.36-4.05-7.16-4.05-5.29,0-9.65,4.67-9.65,10.59a6.91,6.91,0,0,0,.31,2.49A24.86,24.86,0,0,1,5.85,4.75a9.06,9.06,0,0,0-1.55,5.6c0,4.05-.31,7.16,2.18,9-1.56,0-6.23-.62-6.23-1.24,0,5.6,5.6,10,10,10.89-.62.31-.62.63-1.55.63a1.83,1.83,0,0,1-1.25-.32,11.8,11.8,0,0,0,9.34,8.1A19,19,0,0,1,4.61,42.11a4.62,4.62,0,0,1-2.18-.31c4.36,3.11,9.65,5.29,14.94,5,18.06,0,27.71-14.63,27.71-29.27" transform="translate(-0.25 -3.19)"/></svg>
	 		</a>
	 	</li>
	 	<li>
	 		<a href="">
	 			<!-- ICONA SEARCH -->
	 			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.63 43.69"><path d="M45.42,41.53,32,28.13a15.08,15.08,0,0,0,2.89-8.88,2.63,2.63,0,0,0,0-.49,2.83,2.83,0,0,0,0-.5A15.2,15.2,0,0,0,19.73,3.08a3,3,0,0,0-.43,0,3.27,3.27,0,0,0-.44,0A15.2,15.2,0,0,0,3.68,18.26a3.87,3.87,0,0,0,0,.49,4.23,4.23,0,0,0,0,.5A15.2,15.2,0,0,0,18.86,34.43a3.27,3.27,0,0,0,.44,0,3,3,0,0,0,.43,0,15,15,0,0,0,7.76-2.16l13.6,13.6a3.07,3.07,0,1,0,4.33-4.34ZM19.3,28.35a3.13,3.13,0,0,0-.44,0,9.06,9.06,0,0,1-9-9.06,2.63,2.63,0,0,0-.05-.49,2.83,2.83,0,0,0,.05-.5,9.06,9.06,0,0,1,9-9.06,3.13,3.13,0,0,0,.44,0,3.13,3.13,0,0,0,.44,0,9.06,9.06,0,0,1,9.05,9.06,2.63,2.63,0,0,0,.05.49,2.83,2.83,0,0,0-.05.5,9.06,9.06,0,0,1-9.06,9.05Z" transform="translate(-3.68 -3.08)"/></svg>
	 		</a>
	 	</li>
	 </ul>
	 <!-- FINE ICONE FUNZIONALI -->
	</div>
</div>
