<div class="interno">						
	<div class="content-deco">
		<div class="content_details_container">

			<div class="wk_intro_news">
				<div class="wk_wrap_image">
  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action"
  					style="background-image: url('https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/55688_paolovistalligiusta_medium.png');" class="wk_image"></a>
  				</div>
	  			<div class="wk_articolo">
	  				<h1 class="wk_titolo">
	  					<a href="/consulenti-finanziari/reti/55683-dl-rilancio-semplificazione-in-arrivo-per-investire-in-oicr.action">
	  									DL Rilancio, semplificazione in arrivo per investire in OICR
	  					</a>
	  				</h1>
	  				<div class="wk_excerpt">La norma ha carattere eccezionale e regola pertanto i soli contratti conclusi tra la data di entrata in vigore dello stato di emergenza, deliberato dal Consiglio dei ministri il 31 gennaio 2020, e la sua cessazione</div>
	  				<div class="wk_meta_wrap">
	  					<span class="wk_meta">18/05/2020 <span class="wk_sep">|</span> <a href="#">paola.sacerdote</a></span>
	  				</div>
	  				<?php include('block_socialsharing.php');?>
	  			</div>
			</div>

			<div class="wk_news_content">
				<p>La <strong>richiesta di Assogestioni e Assoreti a Mef e Consob di semplificare la sottoscrizione dei contratti d’investimento e di OICR e le comunicazioni agli investitori </strong>nell’attuale fase di emergenza inviata lo scorso aprile non è rimasta inascoltata, ed è&nbsp;stata recepita dal <strong>DL Rilancio</strong>, di cui si attende la pubblicazione in Gazzetta Ufficiale.</p>

				<p>&nbsp;</p>

				<p>
					<h1>Titolo intestazione H1</h1>
					<h2>Titolo intestazione H2</h2>
					<h3>Titolo intestazione H3</h3>
					<h4>Titolo intestazione H4</h4>
					<h5>Titolo intestazione H5</h5>
					<h6>Titolo intestazione H6</h6>
				</p>

				<p>Come riportato da <strong><em>Assogestioni</em></strong>, le disposizioni, introdotte all’<strong>articolo 36</strong>, tengono conto delle difficoltà operative che derivano dalle limitazioni imposte dai recenti Decreti della Presidenza del Consiglio per far fronte alla situazione di emergenza conseguente all’epidemia e mirano ad assicurare la continuità nell’accesso a tali servizi e prodotti da parte degli investitori, agevolando la conclusione a distanza dei nuovi contratti attraverso modalità semplificate di scambio del consenso.</p>

				<p>&nbsp;</p>

				<p>Nella <strong><a href="https://www.advisoronline.it/associazioni/assogestioni/55282-la-lettera-di-assogestioni-e-assoreti-a-mef-e-consob1.action">lettera inviata al MEF e alla Consob lo scorso aprile</a></strong>, Assogestioni e Assoreti chiedevano - sottolinea <strong>Roberta D’Apice</strong>, Direttore del settore legale di Assogestioni - “di prevedere immediatamente per i contratti d’investimento e per l’adesione all’offerta di OICR una norma analoga a quella già introdotta per i contratti bancari e creditizi dall’articolo 4 del DL 8 aprile 2020 n. 23, integrata prevedendo che non soltanto l’esemplare del contratto ma anche la documentazione informativa obbligatoria possa essere messa a disposizione del cliente su un supporto durevole”.</p>

				<p>&nbsp;</p>

				<p>Le misure sono state recepite dall’<strong>articolo 36 del decreto legge Rilancio</strong>, norma che ha <strong>carattere eccezionale </strong>e, pertanto, regola i soli contratti conclusi <strong>tra la data di entrata in vigore dello stato di emergenza (deliberato dal Consiglio dei ministri in data 31 gennaio 2020) e la sua cessazione.</strong> Le nuove misure, pur applicabili ai contratti sottoscritti da tutte le categorie di clienti, operano, come si legge nella relazione illustrativa all’articolo 36, “<strong>principalmente nell’interesse della clientela al dettaglio</strong>, potenzialmente più esposta alle limitazioni imposte dalla crisi nell’accesso ai servizi finanziari, in quanto non sempre in possesso delle dotazioni e strumentazioni informatiche e telematiche necessarie alla conclusione a distanza dei relativi contratti”. In tal modo, precisa la relazione, <blockquote>si conferisce certezza giuridica alle relazioni concluse durante il periodo emergenziale attraverso gli strumenti di comunicazione di più diffuso utilizzo, eliminando il rischio che i relativi contratti risultino affetti da nullità e assicurando agli stessi adeguata efficacia probatoria.</blockquote></p>

				<p>&nbsp;</p>

				<p>La norma prevede che il <strong>consenso del cliente</strong> prestato <strong>mediante posta elettronica non certificata o altro strumento idoneo</strong>, soddisfi il requisito della forma scritta richiesta dal TUF e abbia l’efficacia probatoria di cui all’articolo 2702 del codice civile, a condizione che questi siano accompagnati da copia di un documento di riconoscimento in corso di validità del contraente, facciano riferimento ad un contratto identificabile in modo certo e siano conservati insieme al contratto medesimo con modalità tali da garantirne la sicurezza, l’integrità e l’immodificabilià.</p>

				<p>&nbsp;</p>

				<p>Un <strong>regime speciale</strong> è previsto inoltre per la <strong>consegna da parte dell’intermediario della documentazione contrattuale rilevante</strong>. L’articolo 36 del DL Rilancio precisa che il requisito della consegna di copia del contratto e della documentazione informativa obbligatoria è soddisfatto anche mediante la <strong>messa a disposizione del cliente</strong> di copia del testo del contratto e della documentazione informativa obbligatoria<strong> su “supporto durevole”,</strong> espressione che indica ogni strumento – sia materiale sia elettronico – che permette al consumatore di conservare le informazioni che gli sono personalmente indirizzate in modo da potervi accedere in futuro, e che permette la riproduzione identica delle informazioni memorizzate. L’intermediario è comunque tenuto a consegnare al cliente copia del contratto e della documentazione informativa obbligatoria “alla prima occasione utile successiva al termine dello stato di emergenza”, precisa l’articolo 36 del DL Rilancio.</p>

				<p>&nbsp;</p>

				<p><strong>Fatto salvo anche il diritto di recesso</strong>, con la previsione di un regime speciale relativo alle modalità di esercizio dei diritti di legge o contrattuali da parte del cliente il quale – fino al termine dello stato di emergenza – potrà usare il medesimo supporto impiegato per esprimere il consenso al momento della sottoscrizione del contratto.</p>


				<div class="wk_end_news">
					<h4>
						Hai trovato questa news interessante?<br>
						<strong><i>CONDIVIDILA</i></strong>
					</h4>
					<?php include('block_socialsharing.php');?>
				</div>

				<div class="wk_newsletter_news">
					<div class="wk_fascia_titolo_newsletter">ISCRIVITI ALLA NEWSLETTER</div>
					<div class="wk_newsletter_column">
						Vuoi rimanere aggiornato
						e ricevere news come questa?
						Iscriviti alla nostra newsletter e 
						non perderti tutti gli approfondimenti. 
					</div>
					<div class="wk_newsletter_column wk_input_newsletter">
						<input placeholder="Il tuo indirizzo mail">
						<input type="button" value="iscriviti">
					</div>
				</div>
			</div>


			<!-- NOTIZIE CORRELATE -->
			<!-- ISCRIVITI ALLA NEWSLETTER -->
			<!-- PRIMO PIANO -->


		</div><!-- chiudo content_details_container -->
	</div> <!-- chiudo content-deco -->
</div> <!-- chiudo INTERNO -->