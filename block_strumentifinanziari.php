<!--- INIZIO STRUMENTI FINANZIARI -->


<div class="wrapwidg wk_widget" id="strumenti-finanziari">

	<div class="widget_padding 176380">
		
		<h2 class="widget_title">
			<a href="/tag/view-tags(primo+piano).action">Prodotti finanziari</a>
		</h2>

		<div id="lastContents" class="wk_contenitore_lista">

			<div class="contents_container_all">

				<div class="wk-owl-carousel owl-carousel wk-owl-strumenti-finanziari">

					<?php

					// CICLO PER CONTENUTI RIEMPITIVI

					for($i=0; $i<=11; $i++){

						if($i%4==0){
							
							?><div class="wk_item"><?php
						}
						?>
						  	<div class="wk_sub_item">
						  		<!-- LINK A PAGINA PARTNER -->
  					  			<div class="wk_articolo">
  					  				<a href="#" class="wk_logo_partner">
  					  					<img src="https://d1va1lgf0ctsi4.cloudfront.net/pub/thumb/professional/Credit-Suisse-logo(808).png" title="nome partner" alt="vai alla pagina di NOME DEL PARTNER">
  					  				</a>
  					  				<h4 class="wk_titolo">
  					  					<a href="/consulenti-finanziari/reti/54968-duccio-marconi-il-cf-partner-nell-educazione-finanziaria1.action">Credit Suisse AM lancia nuovi Etf
  					  					</a>
  					  				</h4>
  					  				<div class="wk_excerpt">Introdotti nuovi Etf per venire incontro alla crescente domanda di prodotti...</div>
  					  				<span class="wk_meta">27/03/2020 | <a href="#">Lorenza Roma</a></span>
  					  				<?php include('block_socialsharing.php');?>
  					  			</div>
	  						</div>
						<?php

						if(($i+1)%4==0){;
							?></div><?php
						}
					}

					?>
				</div>


			</div>

			<div class="customContentListFooter">
				<a href="" class="wk_pulsante" title="Tutti i prodotti finanziari" target="_blank">Scopri tutti i prodotti finanziari</a>
			</div>

		</div>	
	</div>
</div>


<!--- FINE STRUMENTI FINANZIARI -->
